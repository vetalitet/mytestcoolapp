package com.vetalitet.mytestcoolapp.services

import com.vetalitet.mytestcoolapp.model.json.RecipeList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RecipeService {

    @GET("recipes/list")
    suspend fun getRecipes(
        @Query("from") from: Int,
        @Query("size") size: Int,
        @Query("tags") tags: String = "under_30_minutes"
    ): Response<RecipeList>

}
