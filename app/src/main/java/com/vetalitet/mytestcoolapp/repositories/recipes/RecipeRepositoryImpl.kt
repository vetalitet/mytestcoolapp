package com.vetalitet.mytestcoolapp.repositories.recipes

import com.vetalitet.mytestcoolapp.model.json.RecipeList
import com.vetalitet.mytestcoolapp.services.RecipeService
import com.vetalitet.mytestcoolapp.utils.ApiException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RecipeRepositoryImpl(private val recipeService: RecipeService): RecipeRepository {

    override suspend fun getRecipes(from: Int, size: Int): RecipeList {
        return withContext(Dispatchers.IO) {
            val response = recipeService.getRecipes(from, size)
            if (response.isSuccessful) {
                response.body() ?: RecipeList(0, mutableListOf())
            } else {
                //val errorBody = response.errorBody()
                throw ApiException(response.errorBody()?.toString() ?: "Unknown Error!")
            }
        }
    }

}
