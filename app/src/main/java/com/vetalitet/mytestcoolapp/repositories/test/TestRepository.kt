package com.vetalitet.mytestcoolapp.repositories.test

import android.util.Log
import com.vetalitet.mytestcoolapp.UiState
import com.vetalitet.mytestcoolapp.model.json.RecipeList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.HttpURLConnection
import java.net.URL

class TestRepository(private val responseParser: TestLoginResponseParser) {

    companion object {
        private const val SERVER_URL = "https://tasty.p.rapidapi.com"
        private const val HEADER_KEY = "x-rapidapi-key"
        //private const val HEADER_VALUE = "479984dae5msh63297ea7ad73ba1p14f899jsn3e91ea23ef28"
        private const val HEADER_VALUE = "1e3e00531cmshff9eeb693d432abp10953djsn55e0306348cc"
        private const val ENDPOINT = "recipes/list"
    }

    fun loadRecipes(page: Int, count: Int): RecipeList {
        /*return withContext(Dispatchers.IO) {
            the second case
        }*/
        val url = URL("$SERVER_URL/$ENDPOINT?rapidapi-key=$HEADER_VALUE&from=${page}&size=${count}&tags=under_30_minutes")
        //val url = URL("http://google.com")
        (url.openConnection() as HttpURLConnection).run {
            requestMethod = "GET"
            /*setRequestProperty("Content-Type", "application/json; utf-8")
            setRequestProperty("Accept", "application/json")
            setRequestProperty(HEADER_KEY, HEADER_VALUE)
            setRequestProperty("x-rapidapi-host", "tasty.p.rapidapi.com")
            setRequestProperty("useQueryString", "true")*/
            responseCode
            return responseParser.parse(inputStream)
        }
    }

}
