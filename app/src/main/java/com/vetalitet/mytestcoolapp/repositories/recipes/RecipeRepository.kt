package com.vetalitet.mytestcoolapp.repositories.recipes

import com.vetalitet.mytestcoolapp.model.json.RecipeList

interface RecipeRepository {

    suspend fun getRecipes(from: Int, size: Int): RecipeList

}