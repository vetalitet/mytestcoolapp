package com.vetalitet.mytestcoolapp.repositories.test

import com.google.gson.Gson
import com.vetalitet.mytestcoolapp.model.json.RecipeList
import java.io.InputStream
import java.io.InputStreamReader
import java.io.Reader
import java.nio.charset.StandardCharsets

class TestLoginResponseParser(private val gson: Gson) {

    fun parse(inputStream: InputStream?): RecipeList {
        val isString = readInputStream(inputStream)
        return gson.fromJson(isString, RecipeList::class.java)
    }

    private fun readInputStream(inputStream: InputStream?): String {
        val bufferSize = 1024
        val buffer = CharArray(bufferSize)
        val out = StringBuilder()
        val `in`: Reader = InputStreamReader(inputStream, StandardCharsets.UTF_8)
        var charsRead: Int
        while (`in`.read(buffer, 0, buffer.size).also { charsRead = it } > 0) {
            out.append(buffer, 0, charsRead)
        }
        return out.toString()
    }
}