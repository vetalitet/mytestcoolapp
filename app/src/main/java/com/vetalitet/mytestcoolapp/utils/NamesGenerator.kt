package com.vetalitet.mytestcoolapp.utils

import com.vetalitet.mytestcoolapp.ui.adapters.RVItem
import kotlin.random.Random

class NamesGenerator {

    companion object {

        private val NAMES = listOf(
            "Liam", "Olivia", "Noah", "Emma",
            "Oliver", "Ava", "William", "Sophia",
            "Elijah", "Isabella", "James", "Charlotte",
            "Benjamin", "Amelia", "Lucas", "Mia",
            "Mason", "Harper", "Ethan", "Evelyn",
            "Patricia", "Michael", "Elizabeth", "Richard",
            "Sarah", "Donald", "Jessica", "Steven"
        )

        private const val ADVERT =
            "Visit Domino's Pizza near you in next two hours and get 25% discount!"

        fun generateNames(count: Int): List<RVItem> {
            val names = mutableListOf<RVItem>()
            for (i in 0 until count) {
                if (i > 0 && i % 7 == 0) {
                    names.add(RVItem(i + 1, ADVERT, 0, true))
                } else {
                    val name = NAMES[Random.nextInt(NAMES.size)]
                    val age = Random.nextInt(40) + 18
                    names.add(RVItem(i + 1, name, age))
                }
            }
            return names
        }

    }

}
