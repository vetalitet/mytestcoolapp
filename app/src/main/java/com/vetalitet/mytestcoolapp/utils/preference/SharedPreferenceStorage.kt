package com.vetalitet.mytestcoolapp.utils.preference

import android.content.SharedPreferences
import androidx.core.content.edit
import com.vetalitet.mytestcoolapp.BuildConfig

class SharedPreferenceStorage(private val sharedPreferences: SharedPreferences) {

    fun saveServerHost(url: String) {
        sharedPreferences.edit(commit = true) {
            putString("host", url)
        }
    }

    fun readServerHost(): String {
        return sharedPreferences.getString("host", BuildConfig.SERVER_URL)!!
    }

}
