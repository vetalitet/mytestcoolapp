package com.vetalitet.mytestcoolapp.utils

import java.text.SimpleDateFormat
import java.util.*

class DateUtils {

    companion object {
        fun toDateString(millis: Long): String {
            val formatter = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
            return formatter.format(Date(millis))
        }
    }

}
