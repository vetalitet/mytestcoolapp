package com.vetalitet.mytestcoolapp.utils

import android.text.TextUtils
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.widget.TextViewCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.vetalitet.mytestcoolapp.R

fun Fragment.snackbar(
    text: CharSequence,
    style: SnackbarStyle = SnackbarStyle.DEFAULT,
    duration: Int = Snackbar.LENGTH_LONG
) =
    Snackbar.make(view!!, text, duration).apply {
        when (style) {
            SnackbarStyle.WARNING -> warningStyle()
            SnackbarStyle.SUCCESS -> successStyle()
            SnackbarStyle.ERROR -> errorStyle()
            SnackbarStyle.DEFAULT -> pindifyStyle()
        }
        show()
    }

enum class SnackbarStyle {
    WARNING, SUCCESS, ERROR, DEFAULT
}

fun Snackbar.warningStyle() = pindifyStyle(R.drawable.ic_warning)
fun Snackbar.successStyle() = pindifyStyle(R.drawable.ic_green_checkmark)
fun Snackbar.errorStyle() = pindifyStyle(R.drawable.ic_forbidden)
fun Snackbar.pindifyStyle(@DrawableRes icon: Int = 0) {
    ViewCompat.setElevation(view, 32.px.toFloat())
    view.background = ContextCompat.getDrawable(context, R.drawable.bg_snackbar)

    val textView = view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
    textView.setTextColor(ContextCompat.getColor(context, R.color.middle_black))
    textView.maxLines = 5
    textView.ellipsize = TextUtils.TruncateAt.END
    textView.compoundDrawablePadding = 16.px

    TextViewCompat.setCompoundDrawablesRelativeWithIntrinsicBounds(textView, icon, 0, 0, 0)
}