package com.vetalitet.mytestcoolapp.utils

import android.content.Context
import androidx.annotation.PluralsRes
import androidx.annotation.StringRes

fun Context.string(@StringRes stringId: Int): String = this.getString(stringId)

fun Context.plural(@PluralsRes stringId: Int, quantity: Int): String = this.resources.getQuantityString(stringId, quantity)
