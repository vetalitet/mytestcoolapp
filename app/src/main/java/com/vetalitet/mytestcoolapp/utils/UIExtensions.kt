package com.vetalitet.mytestcoolapp.utils

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity

fun ActionBar.disableNativeControl(isDisabled: Boolean) {
    this.setDisplayShowTitleEnabled(!isDisabled)
}

fun ActionBar.disableBackArrow(isDisabled: Boolean) {
    this.setDisplayHomeAsUpEnabled(!isDisabled)
    this.setDisplayShowHomeEnabled(!isDisabled)
}

fun AppCompatActivity.updateOpacity(opacity: Int) {
    val colorDrawable = ColorDrawable(Color.BLACK)
    colorDrawable.alpha = opacity
    supportActionBar?.setBackgroundDrawable(colorDrawable)
    window.statusBarColor = colorDrawable.color
}
