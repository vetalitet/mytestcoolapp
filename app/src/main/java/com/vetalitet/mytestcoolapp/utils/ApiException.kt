package com.vetalitet.mytestcoolapp.utils

import java.lang.RuntimeException

class ApiException(message: String) : RuntimeException(message) {
}
