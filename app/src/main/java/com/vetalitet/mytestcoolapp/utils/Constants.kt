package com.vetalitet.mytestcoolapp.utils

const val PREFERENCE_MAIN = "com.vetalitet.app.MAIN"
const val HEADER_KEY = "x-rapidapi-key"
//const val HEADER_VALUE = "479984dae5msh63297ea7ad73ba1p14f899jsn3e91ea23ef28"
const val HEADER_VALUE = "1e3e00531cmshff9eeb693d432abp10953djsn55e0306348cc"

const val UNKNOWN_ERROR = "Unknown error"

const val PAGE_SIZE = 20

val COLORS = listOf("#BA5964", "#503E70")