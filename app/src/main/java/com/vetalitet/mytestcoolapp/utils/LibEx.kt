package com.vetalitet.mytestcoolapp.utils

import android.view.View

internal val View.viewDrawWidth: Int
    get() {
        return measuredWidth - paddingLeft - paddingRight
    }

internal val View.viewDrawHeight: Int
    get() {
        return measuredHeight
    }