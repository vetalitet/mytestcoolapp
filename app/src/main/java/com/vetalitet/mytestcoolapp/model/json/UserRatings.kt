package com.vetalitet.mytestcoolapp.model.json

data class UserRatings(
    val count_negative: Int,
    val count_positive: Int,
    val score: Double
)