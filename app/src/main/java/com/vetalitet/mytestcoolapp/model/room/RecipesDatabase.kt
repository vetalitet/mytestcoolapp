package com.vetalitet.mytestcoolapp.model.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.vetalitet.mytestcoolapp.BuildConfig
import com.vetalitet.mytestcoolapp.model.room.converters.InstructionConverter
import com.vetalitet.mytestcoolapp.model.room.converters.TagConverter
import com.vetalitet.mytestcoolapp.model.room.dao.RecipesDao
import com.vetalitet.mytestcoolapp.model.room.entities.RecipeEntity

@Database(entities = [RecipeEntity::class], version = BuildConfig.VERSION_CODE)
@TypeConverters(InstructionConverter::class, TagConverter::class)
abstract class RecipesDatabase: RoomDatabase() {

    abstract fun getRecipesDao() : RecipesDao

}
