package com.vetalitet.mytestcoolapp.model.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vetalitet.mytestcoolapp.model.json.Instruction
import java.lang.reflect.Type

class InstructionConverter {

    val gson: Gson by lazy { Gson() }

    @TypeConverter
    fun toInstruction(value: String?): List<Instruction>? {
        if (value == null) return null
        val listType: Type = object : TypeToken<ArrayList<Instruction?>?>() {}.type
        return gson.fromJson<List<Instruction>>(value, listType)
    }

    @TypeConverter
    fun toString(value: List<Instruction>?): String? {
        if (value == null) return null
        return gson.toJson(value)
    }

}
