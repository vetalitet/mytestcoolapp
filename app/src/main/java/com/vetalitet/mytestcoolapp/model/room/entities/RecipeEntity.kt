package com.vetalitet.mytestcoolapp.model.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.vetalitet.mytestcoolapp.model.json.Instruction
import com.vetalitet.mytestcoolapp.model.json.Tag

@Entity(tableName = "recipes")
data class RecipeEntity(
    @PrimaryKey
    val id: Int,
    val name: String,
    val thumbnail_url: String,
    val tags: List<Tag>,
    val instructions: List<Instruction>
)
