package com.vetalitet.mytestcoolapp.model.json

data class Tag(
    val display_name: String,
    val id: Int,
    val name: String,
    val type: String
)