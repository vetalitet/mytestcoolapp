package com.vetalitet.mytestcoolapp.model.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.vetalitet.mytestcoolapp.model.room.entities.RecipeEntity

@Dao
interface RecipesDao {

    @Insert(onConflict = REPLACE)
    suspend fun insert(entity: RecipeEntity): Long

    @Query("SELECT * FROM recipes")
    fun getAllRecipes(): LiveData<List<RecipeEntity>>

}
