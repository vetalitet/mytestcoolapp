package com.vetalitet.mytestcoolapp.model.json

data class Instruction(
    val appliance: Any,
    val display_text: String,
    val end_time: Int,
    val id: Int,
    val position: Int,
    val start_time: Int,
    val temperature: Any
)