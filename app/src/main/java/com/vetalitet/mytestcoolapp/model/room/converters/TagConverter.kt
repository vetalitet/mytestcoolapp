package com.vetalitet.mytestcoolapp.model.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.vetalitet.mytestcoolapp.model.json.Tag
import java.lang.reflect.Type

class TagConverter {

    val gson: Gson by lazy { Gson() }

    @TypeConverter
    fun toTag(value: String?): List<Tag>? {
        if (value == null) return null
        val listType: Type = object : TypeToken<ArrayList<Tag?>?>() {}.type
        return gson.fromJson<List<Tag>>(value, listType)
    }

    @TypeConverter
    fun toString(value: List<Tag>?): String? {
        if (value == null) return null
        return gson.toJson(value)
    }

}
