package com.vetalitet.mytestcoolapp.model.json

data class RecipeList(
    val count: Int,
    val results: MutableList<Recipe>
)
