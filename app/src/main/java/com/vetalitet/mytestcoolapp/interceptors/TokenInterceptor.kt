package com.vetalitet.mytestcoolapp.interceptors

import com.vetalitet.mytestcoolapp.utils.HEADER_KEY
import com.vetalitet.mytestcoolapp.utils.HEADER_VALUE
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class TokenInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original: Request = chain.request()

        val request: Request = original.newBuilder()
            .header(HEADER_KEY, HEADER_VALUE)
            .build()

        return chain.proceed(request)
    }

}
