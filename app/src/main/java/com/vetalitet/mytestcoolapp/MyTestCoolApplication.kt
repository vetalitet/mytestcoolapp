package com.vetalitet.mytestcoolapp

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyTestCoolApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            // Android context
            androidContext(this@MyTestCoolApplication)
            // modules
            modules(
                listOf(
                    coreModule,
                    localDataStorageModule,
                    networkModule,
                    repositoryModule,
                    viewModelsModule,
                    databaseModule
                )
            )
        }
    }

}
