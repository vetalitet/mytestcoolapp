package com.vetalitet.mytestcoolapp

sealed class UiState<out T : Any>(open val id: Long? = null) {
    data class Loading(override val id: Long? = null) : UiState<Nothing>()
    data class Error(val message: String? = null, override val id: Long? = null) : UiState<Nothing>()
    data class Success<out T : Any>(val result: T? = null, override val id: Long? = null) : UiState<T>()
}
