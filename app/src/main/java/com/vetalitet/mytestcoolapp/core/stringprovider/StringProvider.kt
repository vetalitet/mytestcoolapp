package com.vetalitet.mytestcoolapp.core.stringprovider

import androidx.annotation.PluralsRes
import androidx.annotation.StringRes

interface StringProvider {
    fun getString(@StringRes stringId: Int): String
    fun getPluralString(@PluralsRes stringId: Int, quantity: Int): String
}
