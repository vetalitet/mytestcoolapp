package com.vetalitet.mytestcoolapp.core.stringprovider

import android.content.Context
import com.vetalitet.mytestcoolapp.utils.plural
import com.vetalitet.mytestcoolapp.utils.string

class StringProviderImpl(private val context: Context) : StringProvider {
    override fun getString(stringId: Int) = context.string(stringId)
    override fun getPluralString(stringId: Int, quantity: Int) = context.plural(stringId, quantity)
}
