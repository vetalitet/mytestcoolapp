package com.vetalitet.mytestcoolapp.core

import androidx.lifecycle.ViewModel

open class BaseViewModel: ViewModel() {}
