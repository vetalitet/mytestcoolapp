package com.vetalitet.mytestcoolapp.features.recipelist

import androidx.lifecycle.viewModelScope
import com.vetalitet.mytestcoolapp.UiState
import com.vetalitet.mytestcoolapp.core.BaseViewModel
import com.vetalitet.mytestcoolapp.model.json.RecipeList
import com.vetalitet.mytestcoolapp.repositories.recipes.RecipeRepository
import com.vetalitet.mytestcoolapp.utils.PAGE_SIZE
import com.vetalitet.mytestcoolapp.utils.SingleLiveEvent
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

class RecipeListViewModel(
    private val recipeRepository: RecipeRepository
) : BaseViewModel() {

    val recipesLiveData = SingleLiveEvent<UiState<RecipeList>>()

    fun loadRecipes() {
        recipesLiveData.postValue(UiState.Loading())
        viewModelScope.launch(CoroutineExceptionHandler { _, throwable ->
            recipesLiveData.postValue(UiState.Error(throwable.message))
        }) {
            val recipes = recipeRepository.getRecipes(0, PAGE_SIZE)
            recipesLiveData.postValue(UiState.Success(recipes))
        }
    }

}
