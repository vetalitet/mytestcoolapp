package com.vetalitet.mytestcoolapp.features.test

import androidx.lifecycle.viewModelScope
import com.vetalitet.mytestcoolapp.UiState
import com.vetalitet.mytestcoolapp.core.BaseViewModel
import com.vetalitet.mytestcoolapp.utils.SingleLiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ShazamViewModel : BaseViewModel() {

    companion object {
        const val MESSAGE = "Exception was thrown while loading song!"
    }

    val loadSongLiveData = SingleLiveEvent<UiState<Unit>>()

    fun loadSong() {
        loadSongLiveData.postValue(UiState.Loading())
        viewModelScope.launch(Dispatchers.IO) {
            try {
                delay(2000)
                loadSongLiveData.postValue(UiState.Success())
            } catch (ex: Exception) {
                loadSongLiveData.postValue(UiState.Error(ex.message ?: MESSAGE))
            }
        }
    }

}
