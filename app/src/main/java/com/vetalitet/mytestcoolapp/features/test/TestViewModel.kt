package com.vetalitet.mytestcoolapp.features.test

import androidx.lifecycle.viewModelScope
import com.vetalitet.mytestcoolapp.UiState
import com.vetalitet.mytestcoolapp.core.BaseViewModel
import com.vetalitet.mytestcoolapp.model.json.RecipeList
import com.vetalitet.mytestcoolapp.repositories.test.TestRepository
import com.vetalitet.mytestcoolapp.utils.PAGE_SIZE
import com.vetalitet.mytestcoolapp.utils.SingleLiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

class TestViewModel(private val testRepository: TestRepository) : BaseViewModel() {

    val recipesLiveData = SingleLiveEvent<UiState<RecipeList>>()

    var page = 0
    var storedRecipesList: RecipeList? = null

    fun loadRecipes() {
        recipesLiveData.postValue(UiState.Loading())
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val recipes = testRepository.loadRecipes(page * PAGE_SIZE, PAGE_SIZE)
                recipesLiveData.postValue(UiState.Success(handleResponse(recipes)))
            } catch (ex: Exception) {
                recipesLiveData.postValue(UiState.Error(ex.message ?: "Unknown error!"))
            }
        }
    }

    private fun handleResponse(recipesList: RecipeList): RecipeList {
        page++
        if (storedRecipesList == null) {
            storedRecipesList = recipesList
        } else {
            val oldRecipes = storedRecipesList?.results
            val newRecipes = recipesList.results
            oldRecipes?.addAll(newRecipes)
        }
        return storedRecipesList ?: recipesList
    }

    fun resetPage() {
        page = 0
    }

}
