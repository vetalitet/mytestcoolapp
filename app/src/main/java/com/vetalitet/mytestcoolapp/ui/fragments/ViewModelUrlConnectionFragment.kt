package com.vetalitet.mytestcoolapp.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.UiState
import com.vetalitet.mytestcoolapp.features.recipelist.RecipeListViewModel
import com.vetalitet.mytestcoolapp.ui.adapters.RecipesDelegatedAdapter
import com.vetalitet.mytestcoolapp.utils.SnackbarStyle
import com.vetalitet.mytestcoolapp.utils.UNKNOWN_ERROR
import com.vetalitet.mytestcoolapp.utils.snackbar
import kotlinx.android.synthetic.main.fragment_view_model.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ViewModelUrlConnectionFragment : BaseFragment(R.layout.fragment_view_model) {

    private val viewModel by viewModel<RecipeListViewModel>()

    private val adapter by lazy {
        RecipesDelegatedAdapter() { position ->
            Log.d("UrlConnectionFragment", "Clicked: $position")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
        setUpRecyclerView();
        viewModel.loadRecipes()
    }

    private fun setUpRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
    }

    private fun observeData() {
        viewModel.recipesLiveData.observe(this, { status ->
            when (status) {
                is UiState.Loading -> {
                    showProgress()
                }
                is UiState.Error -> {
                    snackbar(status.message ?: UNKNOWN_ERROR, SnackbarStyle.ERROR)
                    hideProgress()
                }
                is UiState.Success -> {
                    //adapter.applyItems(status.result?.results ?: emptyList())
                    adapter.data = status.result?.results?.toList() ?: emptyList()
                    hideProgress()
                }
            }
        })
    }

    private fun showProgress() {
        pbLoading.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        pbLoading.visibility = View.GONE
    }

}
