package com.vetalitet.mytestcoolapp.ui.decorators

import android.graphics.Canvas
import android.graphics.Outline
import android.graphics.Rect
import android.view.View
import android.view.ViewOutlineProvider
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import com.vetalitet.mytestcoolapp.ui.adapters.RecyclerViewDecoratorActivityAdapter

class SimpleDividerDifferentViewTypesDecorator: RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        parent.adapter?.let { adapter ->
            val position = parent.getChildAdapterPosition(view).let {
                if (it == RecyclerView.NO_POSITION) return else it
            }
            outRect.bottom = when (adapter.getItemViewType(position)) {
                RecyclerViewDecoratorActivityAdapter.VIEW_TYPE_INFO -> 10
                RecyclerViewDecoratorActivityAdapter.VIEW_TYPE_ADVERT -> 50
                else -> 0
            }
            outRect.top = outRect.bottom
        }
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDraw(c, parent, state)
        parent.children.forEach { view ->
            parent.adapter?.let { adapter ->
                val position = parent.getChildAdapterPosition(view).let {
                    if (it == RecyclerView.NO_POSITION) return else it
                }
                val itemViewType = adapter.getItemViewType(position)
                if (itemViewType == RecyclerViewDecoratorActivityAdapter.VIEW_TYPE_INFO) {
                    val viewOutlineProvider: ViewOutlineProvider = object : ViewOutlineProvider() {
                        override fun getOutline(view: View, outline: Outline) {
                            outline.setRoundRect(
                                0,
                                0,
                                view.width,
                                view.height,
                                (view.height / 3).toFloat()
                            )
                        }
                    }
                    //view.outlineProvider = viewOutlineProvider
                    //view.clipToOutline = true
                }
            }
        }
    }

}
