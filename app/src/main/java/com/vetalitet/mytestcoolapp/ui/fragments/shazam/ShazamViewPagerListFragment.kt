package com.vetalitet.mytestcoolapp.ui.fragments.shazam

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.components.tabs.CustomTabLayout
import com.vetalitet.mytestcoolapp.components.tabs.delegate.ViewPager2Delegate
import com.vetalitet.mytestcoolapp.components.tabs.delegate.ViewPagerToolbarDelegate
import com.vetalitet.mytestcoolapp.ui.adapters.PagerViewAdapter
import com.vetalitet.mytestcoolapp.utils.disableBackArrow
import com.vetalitet.mytestcoolapp.utils.disableNativeControl
import kotlinx.android.synthetic.main.fragment_shazam_viewpager_list.*

class ShazamViewPagerListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_shazam_viewpager_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolbar()
        hideSystemUI()
        setupViewPager(view)
    }

    private fun setupViewPager(view: View) {
        viewPager.adapter = PagerViewAdapter(this)
        val dslTabLayout: CustomTabLayout = view.findViewById(R.id.tabLayout)
        val tabIndicator = dslTabLayout.tabIndicator
        tabIndicator.indicatorDrawable = getDrawable(requireContext(), R.drawable.indicator_bg)
        ViewPager2Delegate.install(view.findViewById(R.id.viewPager), dslTabLayout)
        ViewPagerToolbarDelegate.install(view.findViewById(R.id.viewPager), requestActivity())
    }

    private fun setUpToolbar() {
        requestActivity().setSupportActionBar(toolbar) //set the toolbar
        requestActivity().supportActionBar?.disableNativeControl(true)
        requestActivity().supportActionBar?.disableBackArrow(false)

        ViewCompat.setOnApplyWindowInsetsListener(toolbar) { view, insets ->
            val toolbarParams = toolbar.layoutParams as ConstraintLayout.LayoutParams
            toolbarParams.topMargin = insets.systemWindowInsetTop
            toolbar.layoutParams = toolbarParams
            val viewPagerParams = viewPager.layoutParams as ConstraintLayout.LayoutParams
            viewPagerParams.topMargin = toolbar.height + insets.systemWindowInsetTop
            viewPager.layoutParams = viewPagerParams
            //moviesRecyclerView.updatePadding(top = toolbarHeight + insets.systemWindowInsetTop + baseMoviesPadding)
            insets
        }

        // use it instead of FLAG_TRANSLUCENT_STATUS flag
        //window.statusBarColor = ContextCompat.getColor(this, R.color.dark_transparent)
    }

    private fun hideSystemUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            //window.setDecorFitsSystemWindows(false)
            requestActivity().window.insetsController?.let {
                //it.hide(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
                //it.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            }
        } else {
            @Suppress("DEPRECATION")
            requestActivity().window.decorView.systemUiVisibility = (
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

            // other flags
            // View.SYSTEM_UI_FLAG_FULLSCREEN
            // SYSTEM_UI_FLAG_HIDE_NAVIGATION
            // SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            // SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        }
    }

    private fun requestActivity() = activity as AppCompatActivity

}
