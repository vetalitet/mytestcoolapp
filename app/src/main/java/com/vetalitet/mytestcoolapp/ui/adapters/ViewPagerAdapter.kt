package com.vetalitet.mytestcoolapp.ui.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.vetalitet.mytestcoolapp.ui.fragments.ButtonsFragment
import com.vetalitet.mytestcoolapp.ui.fragments.CustomViewsFragment
import com.vetalitet.mytestcoolapp.ui.fragments.DrawablesFragment
import com.vetalitet.mytestcoolapp.ui.fragments.PlayerFragment
import com.vetalitet.mytestcoolapp.ui.fragments.ViewFragment
import com.vetalitet.mytestcoolapp.ui.fragments.ViewModelFragment
import com.vetalitet.mytestcoolapp.ui.fragments.ViewModelUrlConnectionFragment

class ViewPagerAdapter(activity: FragmentActivity): FragmentStateAdapter(activity) {

    private val fragments = listOf<Fragment>(ViewModelFragment(), ViewModelUrlConnectionFragment(), CustomViewsFragment(), ButtonsFragment(), DrawablesFragment(), ViewFragment(), PlayerFragment())

    override fun getItemCount() = fragments.size

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }

}
