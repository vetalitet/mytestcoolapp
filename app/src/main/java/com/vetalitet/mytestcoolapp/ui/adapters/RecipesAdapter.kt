package com.vetalitet.mytestcoolapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.model.json.Recipe
import kotlinx.android.synthetic.main.item_recipes.view.*

class RecipesAdapter : RecyclerView.Adapter<RecipesViewHolder>() {

    private val items = mutableListOf<Recipe>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipesViewHolder =
        RecipesViewHolder(
            LayoutInflater.from(parent.context.applicationContext)
                .inflate(R.layout.item_recipes, parent, false)
        )

    override fun onBindViewHolder(holder: RecipesViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    fun applyItems(items: List<Recipe>) {
        val diffCallback = DiffCallback(this.items, items)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.items.clear()
        this.items.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }

    inner class DiffCallback(val old: List<Recipe>, val new: List<Recipe>) : DiffUtil.Callback() {
        override fun getOldListSize() = old.size
        override fun getNewListSize() = new.size
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) = old[oldItemPosition].id == new[newItemPosition].id
        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = old[oldItemPosition] == new[newItemPosition]
    }

}

class RecipesViewHolder(private val itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(item: Recipe) {
        itemView.tvName.text = item.name

        Glide.with(itemView.context.applicationContext).load(item.thumbnail_url).into(itemView.ivImage)
    }

}
