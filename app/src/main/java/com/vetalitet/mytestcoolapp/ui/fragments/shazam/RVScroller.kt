package com.vetalitet.mytestcoolapp.ui.fragments.shazam

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.vetalitet.mytestcoolapp.utils.updateOpacity

class RVScroller private constructor(private val builder: Builder) : RecyclerView.OnScrollListener() {

    private var currentScroll: Int = builder.scroll

    private val toolbarHeight = builder.activity.supportActionBar?.height ?: 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        currentScroll += dy
        val difference = currentScroll / toolbarHeight.toFloat()
        val fraction = if (difference < 0.95) difference else 0.95f
        val opacity = (255 * fraction).toInt()

        builder.activity.updateOpacity(opacity)
        builder.scrollCallback(currentScroll, opacity)
    }

    data class Builder(
        val activity: AppCompatActivity,
        val scroll: Int,
        val scrollCallback: (scroll: Int, opacity: Int) -> Unit
    ) {
        fun build() = RVScroller(this)

    }

}
