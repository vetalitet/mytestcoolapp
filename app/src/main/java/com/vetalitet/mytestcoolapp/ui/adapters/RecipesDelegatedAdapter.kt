package com.vetalitet.mytestcoolapp.ui.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.model.json.Recipe
import com.vetalitet.mytestcoolapp.utils.DateUtils
import kotlinx.android.synthetic.main.item_recipes.view.*
import kotlin.properties.Delegates

class RecipesDelegatedAdapter(private val onClick: (Int) -> Unit) :
    RecyclerView.Adapter<RecipesDelegatedViewHolder>() {

    var data: List<Recipe> by Delegates.observable(listOf()) { _, old, new ->
        DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun getOldListSize() = old.size
            override fun getNewListSize() = new.size
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                old[oldItemPosition].id == new[newItemPosition].id

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                old[oldItemPosition] == new[newItemPosition]
        }).dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipesDelegatedViewHolder =
        RecipesDelegatedViewHolder(
            LayoutInflater.from(parent.context.applicationContext)
                .inflate(R.layout.item_recipes, parent, false), onClick
        )

    override fun onBindViewHolder(holder: RecipesDelegatedViewHolder, position: Int) {
        holder.bind(data[position], position)
        Log.d("Inside adapter", "in onBindViewHolder")
    }

    override fun getItemCount() = data.size

}

class RecipesDelegatedViewHolder(private val itemView: View, onClick: (Int) -> Unit) :
    RecyclerView.ViewHolder(itemView) {

    init {
        Log.d("Inside adapter", "set onClickListener")
        itemView.setOnClickListener {
            onClick(absoluteAdapterPosition)
        }
    }

    fun bind(item: Recipe, position: Int) {
        itemView.tvName.text = item.name
        itemView.tvCreatedAt.text = DateUtils.toDateString(item.created_at * 1000)
        Log.d("Inside adapter", "Set bind")

        Glide.with(itemView.context.applicationContext).load(item.thumbnail_url)
            .into(itemView.ivImage)
    }
}

