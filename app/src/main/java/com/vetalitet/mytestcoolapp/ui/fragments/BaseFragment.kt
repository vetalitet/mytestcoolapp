package com.vetalitet.mytestcoolapp.ui.fragments

import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

abstract class BaseFragment @JvmOverloads constructor(@LayoutRes layoutResId: Int = -1) : Fragment(layoutResId) {

}
