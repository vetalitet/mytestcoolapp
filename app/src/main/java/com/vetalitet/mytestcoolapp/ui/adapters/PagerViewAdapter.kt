package com.vetalitet.mytestcoolapp.ui.adapters

import android.util.SparseArray
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.vetalitet.mytestcoolapp.ui.fragments.ViewModelFragment
import com.vetalitet.mytestcoolapp.ui.fragments.shazam.viewpager.ShazamViewPagerFirstFragment

class PagerViewAdapter(fragment: Fragment) :
    FragmentStateAdapter(fragment) {

    val fragments: SparseArray<Fragment> = SparseArray()

    companion object {
        val NAMES = listOf("SONG", "LYRICS", "VIDEO", "ARTIST", "RELATED", "AUTHOR", "YOU")
    }

    override fun getItemCount() = NAMES.size

    override fun createFragment(position: Int): Fragment {
        val fragment = when (position) {
            0 -> ShazamViewPagerFirstFragment()
            //1 -> ViewModelFragment()
            1 -> ShazamViewPagerFirstFragment()
            2 -> ShazamViewPagerFirstFragment()
            3 -> ShazamViewPagerFirstFragment()
            4 -> ShazamViewPagerFirstFragment()
            5 -> ShazamViewPagerFirstFragment()
            6 -> ShazamViewPagerFirstFragment()
            else -> ViewModelFragment()
        }
        fragments.append(position, fragment)
        return fragment
    }

}
