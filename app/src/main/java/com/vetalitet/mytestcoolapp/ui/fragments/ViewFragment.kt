package com.vetalitet.mytestcoolapp.ui.fragments

import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.components.borderimage.BorderDrawable
import com.vetalitet.mytestcoolapp.ui.fragments.utils.BitmapUtils
import kotlinx.android.synthetic.main.fragment_views.*

class ViewFragment : BaseFragment(R.layout.fragment_views) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bitmap = BitmapUtils.decodeSampledBitmapFromResource(resources, R.drawable.ml, 512, 512)
        Log.d("BITMAP_", "Size: (${bitmap.width}, ${bitmap.height})")
        iv.setImageDrawable(BorderDrawable(bitmap))

        initCustomView()
    }

    private fun initCustomView() {
        customViewOval(
            tvCount,
            ContextCompat.getColor(requireContext(), R.color.colorPrimary),
            ContextCompat.getColor(requireContext(), android.R.color.transparent)
        )
        customViewWithRadius(
            customView, ContextCompat.getColor(requireContext(), R.color.colorAccent),
            ContextCompat.getColor(requireContext(), android.R.color.transparent), 90f
        )
    }

    private fun customViewOval(view: View, bgColor: Int, borderColor: Int) {
        val shape = GradientDrawable()
        shape.shape = GradientDrawable.OVAL
        shape.setColor(bgColor)
        shape.setStroke(1, borderColor)
        view.setPadding(10, 10, 10, 10)
        val layoutParams = view.layoutParams
        layoutParams.width = 70
        layoutParams.height = 70
        view.layoutParams = layoutParams
        view.background = shape
    }

    private fun customViewWithRadius(view: View, bgColor: Int, borderColor: Int, radius: Float) {
        val shape = GradientDrawable()
        shape.shape = GradientDrawable.RECTANGLE
        shape.cornerRadius = radius
        shape.setColor(bgColor)
        shape.setStroke(1, borderColor)
        view.setPadding(20, 10, 20, 10)
        view.background = shape
    }

}
