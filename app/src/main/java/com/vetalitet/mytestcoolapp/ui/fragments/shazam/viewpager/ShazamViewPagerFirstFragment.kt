package com.vetalitet.mytestcoolapp.ui.fragments.shazam.viewpager

import android.graphics.drawable.AnimatedVectorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.UiState
import com.vetalitet.mytestcoolapp.features.test.ShazamViewModel
import com.vetalitet.mytestcoolapp.utils.SnackbarStyle
import com.vetalitet.mytestcoolapp.utils.UNKNOWN_ERROR
import com.vetalitet.mytestcoolapp.utils.snackbar
import kotlinx.android.synthetic.main.layout_shazam_test.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ShazamViewPagerFirstFragment : Fragment() {

    private val viewModel by viewModel<ShazamViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_shazam_test, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupDrawables()
        observeData()
        setUpPlayButton()
    }

    private fun setUpPlayButton() {
        buttonPlay.setBackgroundTint(resources.getColor(R.color.colorPrimaryDark))
        buttonPlay.setOnClickListener {
            if (state == PlayButtonState.PAUSE) {
                viewModel.loadSong()
            } else {
                handlePlayButtonClick(PlayButtonState.PLAY)
            }
        }
    }

    private fun observeData() {
        viewModel.loadSongLiveData.observe(this, { status ->
            when (status) {
                is UiState.Loading -> {
                    handlePlayButtonClick(PlayButtonState.LOADING)
                }
                is UiState.Error -> {
                    snackbar(status.message ?: UNKNOWN_ERROR, SnackbarStyle.ERROR)
                    handlePlayButtonClick(PlayButtonState.PLAY)
                }
                is UiState.Success -> {
                    handlePlayButtonClick(PlayButtonState.PAUSE)
                }
            }
        })
    }

    private fun setupDrawables() {
        val drawable = ivPlay.drawable as AnimatedVectorDrawable
        drawable.start()
        DrawableCompat.setTint(ivPlay.background, resources.getColor(android.R.color.white))
    }

    private fun handlePlayButtonClick(inputState: PlayButtonState) {
        when (inputState) {
            PlayButtonState.PAUSE -> {
                state = PlayButtonState.PLAY
                buttonPlay.play()
                buttonPlay.isEnabled = true
            }
            PlayButtonState.PLAY -> {
                state = PlayButtonState.PAUSE
                buttonPlay.pause()
                buttonPlay.isEnabled = true
            }
            else -> {
                state = PlayButtonState.LOADING
                buttonPlay.loading()
                buttonPlay.isEnabled = false
            }
        }
    }

    var state: PlayButtonState = PlayButtonState.PAUSE

    enum class PlayButtonState {
        LOADING,
        PLAY,
        PAUSE
    }

}
