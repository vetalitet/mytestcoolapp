package com.vetalitet.mytestcoolapp.ui.adapters

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vetalitet.mytestcoolapp.R
import kotlinx.android.synthetic.main.item_drawables.view.*

class DrawablesAdapter(private var drawables: List<Drawable>): RecyclerView.Adapter<DrawablesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrawablesViewHolder =
        DrawablesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_drawables, parent, false))

    override fun onBindViewHolder(holder: DrawablesViewHolder, position: Int) {
        holder.bind(drawables[position])
    }

    override fun getItemCount() = drawables.size

    fun updateItems(newDrawables: List<Drawable>) {
        drawables = newDrawables
        notifyDataSetChanged()
    }

}

class DrawablesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(drawable: Drawable) {
        val layoutParams = itemView.ivDrawable.layoutParams
        layoutParams.width = drawable.bounds.width()
        layoutParams.height = drawable.bounds.height()
        itemView.ivDrawable.layoutParams = layoutParams

        itemView.setBackgroundColor(Color.LTGRAY)
        itemView.ivDrawable.setImageDrawable(drawable)
    }

}
