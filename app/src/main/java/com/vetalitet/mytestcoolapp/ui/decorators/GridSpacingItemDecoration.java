package com.vetalitet.mytestcoolapp.ui.decorators;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

    private int spanCount;
    private int spacing;
    private boolean includeEdge;
    private boolean isStaggered;

    public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge, boolean isStaggered) {
        this.spanCount = spanCount;
        this.spacing = spacing;
        this.includeEdge = includeEdge;
        this.isStaggered = isStaggered;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
        int column;
        boolean isFullSpan = false;
        if (isStaggered) {
            StaggeredGridLayoutManager.LayoutParams lp =
                    (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
            column = lp.getSpanIndex(); // item column
            isFullSpan = lp.isFullSpan();
        } else {
            column = position % spanCount;
        }

        if (isFullSpan) {
            outRect.bottom = spacing;
        } else {
            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount;
                outRect.right = (column + 1) * spacing / spanCount;

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                // column * ((1f / spanCount) * spacing)
                outRect.left = column * spacing / spanCount;
                outRect.right = spacing - (column + 1) * spacing / spanCount;
                outRect.bottom = spacing;
            }
        }
    }

    public int getSpacing() {
        return spacing;
    }
}
