package com.vetalitet.mytestcoolapp.ui.fragments

import android.os.Bundle
import android.view.View
import com.google.android.material.tabs.TabLayoutMediator
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.ui.adapters.ViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_menu.tabs
import kotlinx.android.synthetic.main.fragment_menu.viewPager

class MenuFragment : BaseFragment(R.layout.fragment_menu) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initTabs()
    }

    private fun initTabs() {
        activity?.let {
            viewPager.adapter = ViewPagerAdapter(it)
            TabLayoutMediator(
                tabs, viewPager
            ) { tab, position -> tab.text = titles[position] }.attach()
        }
    }

    private val titles = listOf<String>("API Requests", "API Requests2", "Custom views", "Buttons", "Drawables", "Round views", "Player views")

}
