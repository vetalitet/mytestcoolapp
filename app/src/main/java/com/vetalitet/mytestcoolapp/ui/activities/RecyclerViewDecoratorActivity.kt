package com.vetalitet.mytestcoolapp.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.ui.adapters.RecyclerViewDecoratorActivityAdapter
import com.vetalitet.mytestcoolapp.ui.decorators.SimpleDividerDifferentViewTypesDecorator
import com.vetalitet.mytestcoolapp.utils.NamesGenerator.Companion.generateNames
import kotlinx.android.synthetic.main.activity_recycler_view_decorator.*

class RecyclerViewDecoratorActivity: AppCompatActivity() {

    private val adapter by lazy { RecyclerViewDecoratorActivityAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_view_decorator)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider)!!)
        //recyclerView.addItemDecoration(dividerItemDecoration)
        //recyclerView.addItemDecoration(SimpleDividerItemDecorationLastExcluded(10))
        recyclerView.addItemDecoration(SimpleDividerDifferentViewTypesDecorator())

        adapter.applyItems(generateNames(30))
    }

}
