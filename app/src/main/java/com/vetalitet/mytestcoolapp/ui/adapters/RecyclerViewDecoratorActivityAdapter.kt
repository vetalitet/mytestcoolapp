package com.vetalitet.mytestcoolapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vetalitet.mytestcoolapp.R
import kotlinx.android.synthetic.main.item_recycler_view_decor_advert.view.*
import kotlinx.android.synthetic.main.item_recycler_view_decor_info.view.*

class RecyclerViewDecoratorActivityAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_TYPE_INFO = 0
        const val VIEW_TYPE_ADVERT = 1
    }

    private val items = mutableListOf<RVItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_INFO) {
            RVItemInfoViewHolder(
                LayoutInflater.from(parent.context.applicationContext)
                    .inflate(R.layout.item_recycler_view_decor_info, parent, false)
            )
        } else {
            RVItemAdvertViewHolder(
                LayoutInflater.from(parent.context.applicationContext)
                    .inflate(R.layout.item_recycler_view_decor_advert, parent, false)
            )
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RVItemInfoViewHolder) {
            holder.bind(items[position])
        } else if (holder is RVItemAdvertViewHolder) {
            holder.bind(items[position])
        }
    }

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        if (item.isAdvert) {
            return VIEW_TYPE_ADVERT
        }
        return VIEW_TYPE_INFO
    }

    fun applyItems(items: List<RVItem>) {
        val diffCallback = DiffCallback(this.items, items)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.items.clear()
        this.items.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }

    inner class DiffCallback(val old: List<RVItem>, val new: List<RVItem>) : DiffUtil.Callback() {
        override fun getOldListSize() = old.size
        override fun getNewListSize() = new.size
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            old[oldItemPosition].id == new[newItemPosition].id

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            old[oldItemPosition] == new[newItemPosition]
    }

}

class RVItemInfoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(item: RVItem) {
        itemView.tvName.text = item.name
        itemView.tvAge.text = item.age.toString()
    }
}

class RVItemAdvertViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(item: RVItem) {
        itemView.tvAdvertMessage.text = item.name
    }
}

data class RVItem(val id: Int, val name: String, val age: Int, val isAdvert: Boolean = false)
