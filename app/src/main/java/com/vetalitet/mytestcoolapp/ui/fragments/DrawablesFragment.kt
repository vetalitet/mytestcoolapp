package com.vetalitet.mytestcoolapp.ui.fragments

import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.components.customdrawable.CircleDrawable
import com.vetalitet.mytestcoolapp.components.heart.HeartDrawable
import com.vetalitet.mytestcoolapp.components.roundedimage.RoundedDrawable
import com.vetalitet.mytestcoolapp.ui.adapters.DrawablesAdapter
import kotlinx.android.synthetic.main.fragment_drawables.*

class DrawablesFragment : BaseFragment(R.layout.fragment_drawables) {

    private var adapter: DrawablesAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView(prepareDrawableList())
    }

    private fun prepareDrawableList(): List<Drawable> {
        val drawables = mutableListOf<Drawable>()
        drawables.add(getRoundedDrawable())
        drawables.add(getHeartDrawable())
        drawables.add(getCustomCircleDrawable())
        drawables.add(getShapeMatrixDrawable())
        drawables.add(getShapeDrawable())
        return drawables
    }

    private fun getHeartDrawable(): Drawable {
        return HeartDrawable(250)
    }

    private fun getRoundedDrawable(): Drawable {
        val drawable = RoundedDrawable(BitmapFactory.decodeResource(resources, R.drawable.photo1))
        drawable.setBounds(0, 0, 250, 300)
        drawable.setCornerRadius(100f)
        return drawable
    }

    private fun getCustomCircleDrawable(): Drawable {
        val drawable = ResourcesCompat.getDrawable(resources, R.drawable.ic_bell, context?.theme)!!
        val drawableObject = CircleDrawable(drawable, Color.BLUE)
        drawableObject.setBounds(0, 0, 250, 250)
        return drawableObject
    }

    private fun getShapeMatrixDrawable(): Drawable {
        val drawable = ResourcesCompat.getDrawable(resources, R.drawable.shape_rotated, context?.theme)!!
        drawable.setBounds(0, 0, 200, 200)
        return drawable
    }

    private fun getShapeDrawable(): Drawable {
        val drawable = ResourcesCompat.getDrawable(resources, R.drawable.shape, context?.theme)!!
        drawable.setBounds(0, 0, 200, 200)
        return drawable
    }

    private fun setUpRecyclerView(drawables: List<Drawable>) {
        recyclerView.layoutManager = LinearLayoutManager(context)
        adapter = DrawablesAdapter(drawables)
        recyclerView.adapter = adapter
    }

}
