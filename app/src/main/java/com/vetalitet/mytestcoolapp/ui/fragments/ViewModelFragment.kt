package com.vetalitet.mytestcoolapp.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AbsListView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.UiState
import com.vetalitet.mytestcoolapp.features.recipelist.RecipeListViewModel
import com.vetalitet.mytestcoolapp.features.test.TestViewModel
import com.vetalitet.mytestcoolapp.ui.adapters.RecipesDelegatedAdapter
import com.vetalitet.mytestcoolapp.ui.fragments.shazam.RVScroller
import com.vetalitet.mytestcoolapp.utils.PAGE_SIZE
import com.vetalitet.mytestcoolapp.utils.SnackbarStyle
import com.vetalitet.mytestcoolapp.utils.UNKNOWN_ERROR
import com.vetalitet.mytestcoolapp.utils.snackbar
import kotlinx.android.synthetic.main.fragment_view_model.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ViewModelFragment : BaseFragment(R.layout.fragment_view_model) {

    private val viewModel by viewModel<RecipeListViewModel>()
    private val testViewModel by viewModel<TestViewModel>()

    //private val adapter by lazy { RecipesAdapter() }
    private val adapter by lazy {
        RecipesDelegatedAdapter() { position ->
            Log.d("ViewModelFragment", "Clicked: $position")
        }
    }

    private var isScrolling = false

    private var isLoading = false
    private var isLastPage = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
        setUpRecyclerView();
        //viewModel.getRecipes()
        testViewModel.loadRecipes()
    }

    private fun setUpRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
        recyclerView.addOnScrollListener(scrollListener)
    }

    private fun observeData() {
        observeTestDataRepository()
        viewModel.recipesLiveData.observe(this, { status ->
            when (status) {
                is UiState.Loading -> {
                    showProgress()
                }
                is UiState.Error -> {
                    snackbar(status.message ?: UNKNOWN_ERROR, SnackbarStyle.ERROR)
                    hideProgress()
                }
                is UiState.Success -> {
                    //adapter.applyItems(status.result?.results ?: emptyList())
                    adapter.data = status.result?.results?.toList() ?: emptyList()
                    hideProgress()
                }
            }
        })
    }

    private fun observeTestDataRepository() {
        testViewModel.recipesLiveData.observe(this, { status ->
            when (status) {
                is UiState.Loading -> {
                    showProgress()
                }
                is UiState.Error -> {
                    snackbar(status.message ?: UNKNOWN_ERROR, SnackbarStyle.ERROR)
                    hideProgress()
                }
                is UiState.Success -> {
                    hideProgress()
                    //adapter.applyItems(status.result?.results ?: emptyList())
                    status.result?.let { newResponse ->
                        adapter.data = newResponse.results.toList() ?: emptyList()
                        isLastPage = newResponse.results.size == newResponse.count
                    }
                }
            }
        })
    }

    private fun showProgress() {
        pbLoading.visibility = View.VISIBLE
        isLoading = true
    }

    private fun hideProgress() {
        pbLoading.visibility = View.GONE
        isLoading = false
    }

    private val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                isScrolling = true
            }
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val layoutManager = recyclerView.layoutManager as LinearLayoutManager
            val findFirstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
            val visibleItemsCount = layoutManager.childCount
            val totalItemsCount = layoutManager.itemCount

            val isNotLoadingAndNotLastPage = !isLoading && !isLastPage
            val isAtLastItem = findFirstVisibleItemPosition + visibleItemsCount >= totalItemsCount
            val isNotAtBeginning = findFirstVisibleItemPosition >= 0
            val isTotalMoreThanVisible = totalItemsCount >= PAGE_SIZE
            val shouldPaginate = isNotLoadingAndNotLastPage && isAtLastItem && isNotAtBeginning
                    && isTotalMoreThanVisible && isScrolling
            if (shouldPaginate) {
                //viewModel.loadRecipes()
                testViewModel.loadRecipes()
                isScrolling = false
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        testViewModel.resetPage()
    }

}
