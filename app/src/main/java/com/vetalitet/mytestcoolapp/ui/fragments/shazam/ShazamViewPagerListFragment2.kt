package com.vetalitet.mytestcoolapp.ui.fragments.shazam

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.components.tabs.delegate.TabLayoutDelegate
import com.vetalitet.mytestcoolapp.components.tabs.delegate.ViewPagerScaleBlurDelegate
import com.vetalitet.mytestcoolapp.components.tabs.delegate.ViewPagerToolbarDelegate
import com.vetalitet.mytestcoolapp.ui.adapters.PagerViewAdapter
import com.vetalitet.mytestcoolapp.ui.adapters.PagerViewAdapter.Companion.NAMES
import com.vetalitet.mytestcoolapp.utils.disableBackArrow
import com.vetalitet.mytestcoolapp.utils.disableNativeControl
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_shazam_viewpager_list_2.*
import java.util.concurrent.TimeUnit

class ShazamViewPagerListFragment2 : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_shazam_viewpager_list_2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolbar()
        hideSystemUI()
        setupViewPager(view)
        loadImageBackground()

        data class Product(val id: Int, val name: String, val type: String)

        data class ProductDetails(val id: Int, val productId: Int, val width: Int, val height: Int, val weight: Int)

        val PRODUCTS = listOf(
            Product(4, "TV", "Electronics"),
            Product(2, "Table", "Furniture"),
            Product(6, "Chair", "Furniture")
        )

        val PRODUCT_DETAILS = listOf(
            ProductDetails(2, 2, 200, 400, 30),
            ProductDetails(4, 1, 100, 200, 50),
            ProductDetails(6, 3, 300, 600, 40)
        )

        fun getProducts() = PRODUCTS

        fun getProductDetails(productId: Int) = PRODUCT_DETAILS.find { productId == it.productId }

        Log.d("RXJAVA", "Start")
        Observable
            .fromIterable(getProducts())
            .subscribeOn(Schedulers.computation())
            .flatMap {
                Observable
                    .just(it)
                    .delay(it.id.toLong(), TimeUnit.SECONDS)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d("RXJAVA", "Product: ${it.id}, ${it.name}")
            }
    }

    private fun loadImageBackground() {
        Handler(Looper.myLooper()!!).postDelayed({
            ContextCompat.getDrawable(requireContext(), R.drawable.port)?.let { drawable ->
                ivBackground.updateBackground(drawable)
            }
        }, 500)
    }

    private fun setupViewPager(view: View) {
        viewPager.adapter = PagerViewAdapter(this)
        viewPager.offscreenPageLimit = viewPager.adapter?.itemCount ?: 0
        tabLayout.isInlineLabel = true
        tabLayout.tabMode = TabLayout.MODE_SCROLLABLE
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = NAMES[position]
        }.attach()
        TabLayoutDelegate.install(tabLayout, viewPager)
        ViewPagerScaleBlurDelegate.install(view.findViewById(R.id.viewPager), tabLayout) { fromPosition, toPosition, offset ->
            if (fromPosition <= 0 || toPosition <= 0) {
                ivBackground.scaleX = 1 + offset / 10
                ivBackground.scaleY = 1 + offset / 10
            }
            Log.d("PAGE_SCROLL", "fromPosition: $fromPosition, toPosition: $toPosition")
        }
        ViewPagerToolbarDelegate.install(view.findViewById(R.id.viewPager), requestActivity())


    }

    private fun setUpToolbar() {
        requestActivity().setSupportActionBar(toolbar) //set the toolbar
        requestActivity().supportActionBar?.disableNativeControl(true)
        requestActivity().supportActionBar?.disableBackArrow(false)

        ViewCompat.setOnApplyWindowInsetsListener(toolbar) { view, insets ->
            val toolbarParams = toolbar.layoutParams as ConstraintLayout.LayoutParams
            toolbarParams.topMargin = insets.systemWindowInsetTop
            toolbar.layoutParams = toolbarParams
            val viewPagerParams = viewPager.layoutParams as ConstraintLayout.LayoutParams
            viewPagerParams.topMargin = toolbar.height + insets.systemWindowInsetTop
            viewPager.layoutParams = viewPagerParams
            //moviesRecyclerView.updatePadding(top = toolbarHeight + insets.systemWindowInsetTop + baseMoviesPadding)
            insets
        }

        // use it instead of FLAG_TRANSLUCENT_STATUS flag
        //window.statusBarColor = ContextCompat.getColor(this, R.color.dark_transparent)
    }

    private fun hideSystemUI() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            //window.setDecorFitsSystemWindows(false)
            requestActivity().window.insetsController?.let {
                //it.hide(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
                //it.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            }
        } else {
            @Suppress("DEPRECATION")
            requestActivity().window.decorView.systemUiVisibility = (
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

            // other flags
            // View.SYSTEM_UI_FLAG_FULLSCREEN
            // SYSTEM_UI_FLAG_HIDE_NAVIGATION
            // SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            // SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        }
    }

    private fun requestActivity() = activity as AppCompatActivity

}
