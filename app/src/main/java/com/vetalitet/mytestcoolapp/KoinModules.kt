package com.vetalitet.mytestcoolapp

import android.content.Context
import android.content.pm.ApplicationInfo
import androidx.room.Room
import com.google.gson.Gson
import com.vetalitet.mytestcoolapp.core.stringprovider.StringProvider
import com.vetalitet.mytestcoolapp.core.stringprovider.StringProviderImpl
import com.vetalitet.mytestcoolapp.features.recipelist.RecipeListViewModel
import com.vetalitet.mytestcoolapp.features.test.ShazamViewModel
import com.vetalitet.mytestcoolapp.features.test.TestViewModel
import com.vetalitet.mytestcoolapp.interceptors.TokenInterceptor
import com.vetalitet.mytestcoolapp.model.room.RecipesDatabase
import com.vetalitet.mytestcoolapp.repositories.recipes.RecipeRepository
import com.vetalitet.mytestcoolapp.repositories.recipes.RecipeRepositoryImpl
import com.vetalitet.mytestcoolapp.repositories.test.TestLoginResponseParser
import com.vetalitet.mytestcoolapp.repositories.test.TestRepository
import com.vetalitet.mytestcoolapp.services.RecipeService
import com.vetalitet.mytestcoolapp.utils.PREFERENCE_MAIN
import com.vetalitet.mytestcoolapp.utils.preference.SharedPreferenceStorage
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.TimeUnit

val coreModule = module {
    single<StringProvider> { StringProviderImpl(get()) }
    single { TestLoginResponseParser(get()) }
}

val localDataStorageModule = module {
    single { get<Context>().getSharedPreferences(PREFERENCE_MAIN, Context.MODE_PRIVATE) }
    single { SharedPreferenceStorage(get()) }
}

val networkModule = module {
    single { Gson() }
    single<Interceptor> { TokenInterceptor() }
    single<OkHttpClient> { createOkHttp(get(), get()) }
    single<Retrofit> { provideRetrofit(get(), get()) }
    single<RecipeService> { retrofitService<RecipeService>(get()) }
}

val repositoryModule = module {
    single<RecipeRepository> { RecipeRepositoryImpl(get()) }
    single { TestRepository(get()) }
}

val viewModelsModule = module {
    viewModel { RecipeListViewModel(get()) }
    viewModel { TestViewModel(get()) }
    viewModel { ShazamViewModel() }
}

val databaseModule = module {
    single { provideRecipesDatabase(get()) }
    single { get<RecipesDatabase>().getRecipesDao() }
}

private fun provideRetrofit(client: OkHttpClient, sharedPreferenceStorage: SharedPreferenceStorage) =
    Retrofit.Builder()
        .baseUrl(sharedPreferenceStorage.readServerHost())
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

private fun createOkHttp(context: Context, responseInterceptor: Interceptor): OkHttpClient {
    val isDebuggable = (0 != (context.applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE))
    val interceptor = HttpLoggingInterceptor()
    interceptor.level = if (isDebuggable) Level.BODY else Level.BODY
    val cookieManager = CookieManager()
    cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
    return OkHttpClient.Builder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        //.cookieJar(JavaNetCookieJar(cookieManager))
        .addInterceptor(responseInterceptor)
        .addInterceptor(interceptor)
        .retryOnConnectionFailure(true)
        .build()
}

private fun provideRecipesDatabase(context: Context): RecipesDatabase =
    Room.databaseBuilder(context, RecipesDatabase::class.java, "recipes_db")
        .build()

private inline fun <reified T> retrofitService(retrofit: Retrofit) = retrofit.create(T::class.java)
