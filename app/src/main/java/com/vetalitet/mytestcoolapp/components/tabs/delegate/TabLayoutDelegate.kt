package com.vetalitet.mytestcoolapp.components.tabs.delegate

import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import kotlin.math.absoluteValue

class TabLayoutDelegate(tabLayout: TabLayout, viewPager: ViewPager2) {

    var fromPosition = 0

    companion object {
        fun install(tabLayout: TabLayout, viewPager: ViewPager2) {
            TabLayoutDelegate(tabLayout, viewPager)
        }
    }

    init {
        tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.position?.let { toPosition ->
                    viewPager.setCurrentItem(toPosition, (fromPosition - toPosition).absoluteValue <= 1)
                    fromPosition = toPosition
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

        })
    }

}
