package com.vetalitet.mytestcoolapp.components.progressview

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import androidx.annotation.ColorInt
import com.vetalitet.mytestcoolapp.R

class ProgressViewComp @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    companion object {
        private const val DEFAULT_BG_OVAL_COLOR = Color.GRAY
        private const val DEFAULT_FRONT_OVAL_COLOR = Color.RED
        private const val DEFAULT_BORDER_WIDTH = 10
    }

    @ColorInt
    private var bgOvalColor: Int = getDefaultColor(DEFAULT_BG_OVAL_COLOR)

    @ColorInt
    private var frontOvalColor: Int = getDefaultColor(DEFAULT_FRONT_OVAL_COLOR)

    private val bgOvalPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = bgOvalColor
    }

    private val frontOvalPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = frontOvalColor
        strokeCap = Paint.Cap.ROUND
    }

    private var strokeWidth = DEFAULT_BORDER_WIDTH

    init {
        readAttributesAndSetupFields(context, attrs, defStyleAttr)
    }

    private fun readAttributesAndSetupFields(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) {
        val attributes = context.obtainStyledAttributes(
            attrs, R.styleable.ProgressViewComp, defStyleAttr, 0
        )

        applyAttributes(context, attributes)
        updatePaint()

        attributes.recycle()
    }

    private fun updatePaint() {
        bgOvalPaint.color = bgOvalColor
        bgOvalPaint.strokeWidth = strokeWidth.toFloat()
        frontOvalPaint.color = frontOvalColor
        frontOvalPaint.strokeWidth = strokeWidth.toFloat()
    }

    private fun applyAttributes(context: Context, a: TypedArray) {
        readBgOvalColor(a)
        readFrontOvalColor(a)
        strokeWidth = a.getDimensionPixelSize(
            R.styleable.ProgressViewComp_pvc_border_width, DEFAULT_BORDER_WIDTH
        )
    }

    private fun readFrontOvalColor(attributes: TypedArray) {
        val colorStateList =
            attributes.getColorStateList(R.styleable.ProgressViewComp_pvc_front_oval_color)

        frontOvalColor = colorStateList?.defaultColor ?: getDefaultColor(DEFAULT_FRONT_OVAL_COLOR)
    }

    private fun readBgOvalColor(attributes: TypedArray) {
        val colorStateList =
            attributes.getColorStateList(R.styleable.ProgressViewComp_pvc_bg_oval_color)

        bgOvalColor = colorStateList?.defaultColor ?: getDefaultColor(DEFAULT_BG_OVAL_COLOR)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val size = resolveDefaultSize(widthMeasureSpec)
        setMeasuredDimension(size, size)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawCircle(width / 2f, height / 2f, width / 2f - strokeWidth / 2f, bgOvalPaint)
        canvas.drawArc(
            strokeWidth / 2f,
            strokeWidth / 2f,
            width - strokeWidth / 2f,
            height - strokeWidth / 2f,
            0f,
            90f,
            false,
            frontOvalPaint
        )
    }

    private fun resolveDefaultSize(spec: Int): Int {
        return when (MeasureSpec.getMode(spec)) {
            MeasureSpec.UNSPECIFIED -> 100
            MeasureSpec.AT_MOST -> MeasureSpec.getSize(spec)
            MeasureSpec.EXACTLY -> MeasureSpec.getSize(spec)
            else -> MeasureSpec.getSize(spec)
        }
    }

    private fun getDefaultColor(color: Int) = ColorStateList.valueOf(color).defaultColor

}
