package com.vetalitet.mytestcoolapp.components.imageviews

import android.animation.ValueAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.animation.LinearInterpolator

class BouncyObservingPlayButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : PlayButton(context, attrs, defStyleAttr) {

    private val downAnimator: ValueAnimator by lazy {
        createValueAnimator(1f)
    }

    private val upAnimator: ValueAnimator by lazy {
        createValueAnimator(0.9f)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (playButtonState == PlayButtonState.Loading) return super.onTouchEvent(event)

        val action = event.action
        if (action == MotionEvent.ACTION_DOWN) {
            downAnimation()
        } else if (action == MotionEvent.ACTION_UP) {
            upAnimation()
        }
        return super.onTouchEvent(event)
    }

    private fun createValueAnimator(initValue: Float): ValueAnimator {
        val sign = if (initValue < 1) -1.0f else 1.0f
        return ValueAnimator.ofFloat(0f, 0.1f).apply {
            interpolator = LinearInterpolator()
            duration = 150
            addUpdateListener {
                scaleX = (initValue - (sign * it.animatedValue as Float))
                scaleY = (initValue - (sign * it.animatedValue as Float))
            }
        }
    }

    private fun downAnimation() {
        upAnimator.cancel()
        downAnimator.start()
    }

    private fun upAnimation() {
        downAnimator.cancel()
        upAnimator.start()
    }

}
