package com.vetalitet.mytestcoolapp.components.roundedimage

import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.BitmapShader
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.ColorFilter
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.Shader.TileMode
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.util.Log
import androidx.annotation.ColorInt

class RoundedDrawable constructor(
    private val bitmap: Bitmap
) : Drawable() {

    private var mBorderColor = ColorStateList.valueOf(DEFAULT_BORDER_COLOR)
    private val mBitmapPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
    }
    private val mBorderPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = mBorderColor.getColorForState(state, DEFAULT_BORDER_COLOR)
        strokeWidth = mBorderWidth
    }

    private var mBorderWidth = 0f
    private var mBitmapWidth = 0
    private var mBitmapHeight = 0
    private val mBitmapRect = RectF()
    private val mBorderRect = RectF()
    private val mSquareCornersRect = RectF()

    private var rebuildShader: Boolean = true
    private val mShaderMatrix = Matrix()
    private val tileModeX = TileMode.CLAMP
    private val tileModeY = TileMode.CLAMP

    private val mDrawableRect = RectF()
    private var mCornerRadius = 0f
    private val bounds = RectF()

    init {
        mBitmapWidth = bitmap.width
        mBitmapHeight = bitmap.height
        mBitmapRect.set(0f, 0f, mBitmapWidth.toFloat(), mBitmapHeight.toFloat())
    }

    override fun onBoundsChange(bounds: Rect) {
        super.onBoundsChange(bounds)
        this.bounds.set(bounds)
        updateShaderMatrix()
    }

    override fun draw(canvas: Canvas) {
        if (rebuildShader) {
            val bitmapShader = BitmapShader(bitmap, tileModeX, tileModeY)
            if (tileModeX == TileMode.CLAMP && tileModeY == TileMode.CLAMP) {
                bitmapShader.setLocalMatrix(mShaderMatrix)
            }
            mBitmapPaint.shader = bitmapShader
            rebuildShader = false
        }

        canvas.drawRoundRect(mDrawableRect, mCornerRadius, mCornerRadius, mBitmapPaint)
        if (mBorderWidth > 0) {
            canvas.drawRoundRect(mBorderRect, mCornerRadius, mCornerRadius, mBorderPaint)
        }

        if (mCornerRadius > 0 && mBorderWidth > 0) {
            redrawBitmapForSquareCorners(canvas)
            redrawBorderForSquareCorners(canvas)
        }
    }

    private fun redrawBorderForSquareCorners(canvas: Canvas) {
        val left = mDrawableRect.left
        val top = mDrawableRect.top
        val right = left + mDrawableRect.width()
        val radius = mCornerRadius

        canvas.drawLine(left, top, left + radius, top, mBorderPaint)
        canvas.drawLine(left, top, left, top + radius, mBorderPaint)
        canvas.drawLine(right, top, right, top + radius, mBorderPaint)
    }

    private fun redrawBitmapForSquareCorners(canvas: Canvas) {
        val left = mDrawableRect.left
        val top = mDrawableRect.top
        val radius = mCornerRadius

        mSquareCornersRect.set(left, top, left + radius, top + radius)
        canvas.drawRect(mSquareCornersRect, mBitmapPaint)
    }

    override fun setAlpha(alpha: Int) {
        mBitmapPaint.alpha = alpha
        invalidateSelf()
    }

    override fun setColorFilter(cf: ColorFilter?) {
        mBitmapPaint.colorFilter = cf
        invalidateSelf()
    }

    override fun getOpacity(): Int = PixelFormat.TRANSLUCENT

    private fun updateShaderMatrix() {
        mBorderRect.set(bounds)
        mBorderRect.inset(mBorderWidth / 2, mBorderWidth / 2)
        mDrawableRect.set(mBorderRect)

        val scale: Float
        var dx = 0f
        var dy = 0f

        if (mBitmapWidth * mBorderRect.height() > mBorderRect.width() * mBitmapHeight) {
            scale = mBorderRect.height() / mBitmapHeight.toFloat()
            dx = (mBorderRect.width() - mBitmapWidth * scale) * 0.5f
        } else {
            scale = mBorderRect.width() / mBitmapWidth.toFloat()
            dy = (mBorderRect.height() - mBitmapHeight * scale) * 0.5f
        }

        mShaderMatrix.setScale(scale, scale)
        mShaderMatrix.postTranslate(dx + 0.5f, dy + 0.5f)
    }

    // getters/setters

    fun getBorderWidth() = mBorderWidth

    fun setBorderWidth(width: Float): RoundedDrawable {
        mBorderWidth = width
        mBorderPaint.strokeWidth = mBorderWidth
        return this
    }

    fun getBorderColor() = mBorderColor.defaultColor

    fun setBorderColor(@ColorInt color: Int): RoundedDrawable {
        return setBorderColor(ColorStateList.valueOf(color))
    }

    fun getBorderColors() = mBorderColor

    fun setBorderColor(colors: ColorStateList?): RoundedDrawable {
        mBorderColor = colors ?: ColorStateList.valueOf(0)
        mBorderPaint.color = mBorderColor.getColorForState(state, DEFAULT_BORDER_COLOR)
        return this
    }

    fun getCornerRadius() = mCornerRadius

    fun setCornerRadius(radius: Float): RoundedDrawable {
        mCornerRadius = radius
        return this
    }

    companion object {
        private const val DEFAULT_BORDER_COLOR = Color.BLACK
        fun createFromBitmap(bitmap: Bitmap?): RoundedDrawable? {
            return bitmap?.let { RoundedDrawable(it) }
        }

        fun createFromDrawable(drawable: Drawable?): Drawable? {
            if (drawable != null) {
                if (drawable is RoundedDrawable) {
                    // just return if it's already a RoundedDrawable
                    return drawable
                } else if (drawable is LayerDrawable) {
                    val ld = drawable
                    val num = ld.numberOfLayers

                    // loop through layers to and change to RoundedDrawables if possible
                    for (i in 0 until num) {
                        val d = ld.getDrawable(i)
                        ld.setDrawableByLayerId(ld.getId(i), createFromDrawable(d))
                    }
                    return ld
                }

                // try to get a bitmap from the drawable and
                val bm = drawableToBitmap(drawable)
                if (bm != null) {
                    return RoundedDrawable(bm)
                }
            }
            return drawable
        }

        private fun drawableToBitmap(drawable: Drawable): Bitmap? {
            if (drawable is BitmapDrawable) {
                return drawable.bitmap
            }
            var bitmap: Bitmap?
            val width = Math.max(drawable.intrinsicWidth, 2)
            val height = Math.max(drawable.intrinsicHeight, 2)
            try {
                bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(bitmap)
                drawable.setBounds(0, 0, canvas.width, canvas.height)
                drawable.draw(canvas)
            } catch (e: Exception) {
                e.printStackTrace()
                Log.w(
                    "RoundedDrawable",
                    "Failed to create bitmap from drawable!"
                )
                bitmap = null
            }
            return bitmap
        }
    }

}
