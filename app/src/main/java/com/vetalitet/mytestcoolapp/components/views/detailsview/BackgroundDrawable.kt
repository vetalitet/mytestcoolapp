package com.vetalitet.mytestcoolapp.components.views.detailsview

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.Rect
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.util.Log
import kotlin.math.max

class BackgroundDrawable(private val bitmap: Bitmap): Drawable() {

    private val mBitmapPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val mShaderMatrix = Matrix()
    private val destRect = Rect()

    private var mBitmapWidth = 0
    private var mBitmapHeight = 0

    private lateinit var mBackgroundBitmap: Bitmap

    init {
        mBitmapWidth = bitmap.width
        mBitmapHeight = bitmap.height
    }

    override fun onBoundsChange(bounds: Rect) {
        super.onBoundsChange(bounds)
        updateShaderMatrix()
        prepareBackgroundBitmap()
        Log.d(TAG, "bounds: (${bounds.width()}, ${bounds.height()})")
    }

    private fun prepareBackgroundBitmap() {
        mBackgroundBitmap = Bitmap.createBitmap(bitmap, 0, 0, mBitmapWidth, mBitmapHeight, mShaderMatrix, false)
        val posX = (mBackgroundBitmap.width - bounds.width()) / 2
        val posY = (mBackgroundBitmap.height - bounds.height()) / 2
        mBackgroundBitmap = Bitmap.createBitmap(mBackgroundBitmap, posX, posY, bounds.width(), bounds.height())
        Log.d(TAG, "bitmap: (${bitmap.width}, ${bitmap.height})")
        Log.d(TAG, "mDrawBitmap: (${mBackgroundBitmap.width}, ${mBackgroundBitmap.height})")
    }

    private fun updateShaderMatrix() {
        val scale: Float
        var dx = 0f
        var dy = 0f

        if (mBitmapWidth * bounds.height() > bounds.width() * mBitmapHeight) {
            scale = bounds.height() / mBitmapHeight.toFloat()
            dx = (bounds.width() - mBitmapWidth * scale) * 0.5f
        } else {
            scale = bounds.width() / mBitmapWidth.toFloat()
            dy = (bounds.height() - mBitmapHeight * scale) * 0.5f
        }

        mShaderMatrix.setScale(scale, scale)
        mShaderMatrix.postTranslate(dx + 0.5f, dy + 0.5f)
    }

    override fun draw(canvas: Canvas) {
        destRect.set(0, 0, bounds.width() / 2, bounds.height() / 2)
        canvas.drawBitmap(mBackgroundBitmap, destRect, destRect, null)
    }

    override fun setAlpha(p0: Int) {
        TODO("Not yet implemented")
    }

    override fun setColorFilter(p0: ColorFilter?) {
        TODO("Not yet implemented")
    }

    override fun getOpacity() = PixelFormat.TRANSLUCENT

    companion object {
        private const val TAG = "BackgroundDrawable"

        fun fromDrawable(drawable: Drawable?): Drawable? {
            if (drawable != null) {
                if (drawable is BackgroundDrawable) {
                    return drawable
                } else if (drawable is LayerDrawable) {
                    val cs = drawable.mutate().constantState
                    val ld = (cs?.newDrawable() ?: drawable) as LayerDrawable
                    val num = ld.numberOfLayers

                    // loop through layers to and change to RoundedDrawables if possible
                    for (i in 0 until num) {
                        val d = ld.getDrawable(i)
                        ld.setDrawableByLayerId(ld.getId(i), fromDrawable(d))
                    }
                    return ld
                }

                // try to get a bitmap from the drawable and
                val bm = drawableToBitmap(drawable)
                if (bm != null) {
                    return BackgroundDrawable(bm)
                }
            }
            return drawable
        }

        private fun drawableToBitmap(drawable: Drawable): Bitmap? {
            if (drawable is BitmapDrawable) {
                return drawable.bitmap
            }
            var bitmap: Bitmap?
            val width = max(drawable.intrinsicWidth, 2)
            val height = max(drawable.intrinsicHeight, 2)
            try {
                bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(bitmap)
                drawable.setBounds(0, 0, canvas.width, canvas.height)
                drawable.draw(canvas)
            } catch (e: Throwable) {
                e.printStackTrace()
                Log.w(
                    BackgroundDrawable.toString(),
                    "Failed to create bitmap from drawable!"
                )
                bitmap = null
            }
            return bitmap
        }
    }

}
