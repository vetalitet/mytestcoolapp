package com.vetalitet.mytestcoolapp.components.heart

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.ColorFilter
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable


class HeartDrawable(private val size: Int): Drawable() {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        strokeJoin = Paint.Join.MITER
        color = Color.RED
        strokeWidth = 15f
        style = Paint.Style.STROKE
    }

    private val path = Path()

    init {
        bounds.set(0, 0, size, size)
    }

    override fun draw(canvas: Canvas) {
        canvas.drawColor(Color.WHITE)
        paint.shader = null

        val width = size.toFloat()
        val height = size.toFloat()

        // Starting point

        // Starting point
        path.moveTo(width / 2, height / 5)

        // Upper left path

        // Upper left path
        path.cubicTo(
            5 * width / 14, 0f, 0f, height / 15,
            width / 28, 2 * height / 5
        )

        // Lower left path

        // Lower left path
        path.cubicTo(
            width / 14, 2 * height / 3,
            3 * width / 7, 5 * height / 6,
            width / 2, height
        )

        // Lower right path

        // Lower right path
        path.cubicTo(
            4 * width / 7, 5 * height / 6,
            13 * width / 14, 2 * height / 3,
            27 * width / 28, 2 * height / 5
        )

        // Upper right path

        // Upper right path
        path.cubicTo(
            width, height / 15,
            9 * width / 14, 0f,
            width / 2, height / 5
        )

        paint.color = Color.RED
        paint.style = Paint.Style.FILL
        canvas.drawPath(path, paint)
    }

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        paint.colorFilter = colorFilter
    }

    override fun getOpacity() = PixelFormat.TRANSLUCENT

}
