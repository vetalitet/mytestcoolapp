package com.vetalitet.mytestcoolapp.components.background

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.GradientDrawable.Orientation.TOP_BOTTOM
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.core.graphics.ColorUtils
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.utils.COLORS
import kotlinx.android.synthetic.main.layout_fragment_background.view.*
import kotlin.random.Random


class FragmentBackground @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_fragment_background, this)
        val bgColorImageView = view.findViewById<ImageView>(R.id.ivBackgroundColor)
        val randomColor = COLORS[Random.nextInt(COLORS.size)]
        val startColor = ColorUtils.setAlphaComponent(Color.parseColor(randomColor), 170) // AA
        val centerColor = ColorUtils.setAlphaComponent(Color.parseColor(randomColor), 221) // DD
        val gd = GradientDrawable(
            TOP_BOTTOM, intArrayOf(
                startColor,
                centerColor, android.R.color.black
            )
        )
        bgColorImageView.background = gd
    }

    fun updateBackground(drawable: Drawable) {
        ivBackgroundImage.setImageDrawable(drawable)
    }

}