package com.vetalitet.mytestcoolapp.components.tabs

import android.content.Context
import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import android.text.TextPaint
import android.util.AttributeSet
import android.util.Log
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.utils.dp
import com.vetalitet.mytestcoolapp.utils.tintDrawableColor
import com.vetalitet.mytestcoolapp.utils.viewDrawHeight
import com.vetalitet.mytestcoolapp.utils.viewDrawWidth
import kotlin.math.max

class TabIndicator(private val tabLayout: CustomTabLayout) : Drawable() {

    val textPaint: TextPaint by lazy {
        TextPaint(Paint.ANTI_ALIAS_FLAG).apply {
            isFilterBitmap = true
            style = Paint.Style.FILL
            textSize = 12 * dp
        }
    }

    var indicatorColor: Int = -1
        set(value) {
            field = value
            indicatorDrawable = indicatorDrawable
        }

    var indicatorDrawable: Drawable? = null
        set(value) {
            val tintDrawableColor = tintDrawableColor(value, indicatorColor)
            field = tintDrawableColor
        }

    var positionOffset: Float = 0f
        set(value) {
            field = value
            Log.d("onPageScrolled", "change positionOffset")
            //invalidateSelf()
        }

    var indicatorWidthOffset = 0
    var currentIndex = -1
        set(value) {
            field = value
            invalidateSelf()
        }
    var targetIndex = -1

    fun initAttributes(context: Context, attrs: AttributeSet?) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomTabLayout)
        indicatorColor =
            typedArray.getColor(R.styleable.CustomTabLayout_ctl_indicator_color, indicatorColor)
        indicatorDrawable =
            typedArray.getDrawable(R.styleable.CustomTabLayout_ctl_indicator_drawable)
        indicatorWidthOffset =
            typedArray.getDimensionPixelOffset(
                R.styleable.CustomTabLayout_ctl_indicator_width_offset,
                indicatorWidthOffset
            )
        typedArray.recycle()
    }

    override fun draw(canvas: Canvas) {
        val childSize = tabLayout.itemSelector.visibleViewList.size

        Log.d("onPageScrolled", "TabIndicator onDraw")

        var currentIndex = currentIndex

        if (targetIndex in 0 until childSize) {
            currentIndex = max(0, currentIndex)
        }

        if (currentIndex !in 0 until childSize) {
            return
        }

        val drawCenterX = getChildCenterX(currentIndex)
        val drawWidth = getIndicatorDrawWidth(currentIndex)

        val drawLeft = drawCenterX - drawWidth / 2/* + indicatorXOffset*/

        var animLeft = drawLeft
        var animWidth = drawWidth

        if (targetIndex != currentIndex) {
            val animStartLeft = drawLeft
            val animStartWidth = drawWidth
            val animEndWidth = getIndicatorDrawWidth(targetIndex)
            val animEndLeft = getChildCenterX(targetIndex) - animEndWidth / 2/* + indicatorXOffset*/
            if (targetIndex > currentIndex) {
                animLeft =
                    (animStartLeft + (animEndLeft - animStartLeft) * positionOffset).toInt()
            } else {
                animLeft =
                    (animStartLeft - (animStartLeft - animEndLeft) * positionOffset).toInt()
            }

            animWidth =
                (animStartWidth + (animEndWidth - animStartWidth) * positionOffset).toInt()
        }

        Log.d("TAG_TTT", "animLeft: $animLeft")

        indicatorDrawable?.apply {
            setBounds(
                animLeft,
                0,
                animLeft + animWidth,
                getIndicatorDrawHeight(targetIndex)
            )
            draw(canvas)
        }
    }

    fun getChildCenterX(index: Int): Int {
        var result = 0
        tabLayout.itemSelector.visibleViewList.getOrNull(index)?.also { childView ->
            result = childView.left + childView.paddingLeft + childView.viewDrawWidth / 2
        }
        return result
    }

    private fun getIndicatorDrawWidth(index: Int): Int {
        var result = 0
        tabLayout.itemSelector.visibleViewList.getOrNull(index)?.also { childView ->
            result = childView.viewDrawWidth
        }
        return result + indicatorWidthOffset
    }

    private fun getIndicatorDrawHeight(index: Int): Int {
        var result = 0
        tabLayout.itemSelector.visibleViewList.getOrNull(index)?.also { childView ->
            result = childView.viewDrawHeight
        }
        return result
    }

    override fun setAlpha(alpha: Int) {
        if (textPaint.alpha != alpha) {
            textPaint.alpha = alpha
            invalidateSelf()
        }
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        textPaint.colorFilter = colorFilter
        invalidateSelf()
    }

    override fun getOpacity(): Int {
        return if (alpha < 255) PixelFormat.TRANSLUCENT else PixelFormat.OPAQUE
    }

    private fun tintDrawableColor(drawable: Drawable?, color: Int): Drawable? {
        if (drawable == null) {
            return drawable
        }
        return drawable.tintDrawableColor(color)
    }

}
