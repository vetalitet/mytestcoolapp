package com.vetalitet.mytestcoolapp.components.tabs

import android.view.View
import android.view.ViewGroup

class ItemSelector {

    var selectIndex: Int = -1
    var parent: ViewGroup? = null
    var itemSelector: ItemSelectorCallback = ItemSelectorCallback()

    val visibleViewList: MutableList<View> = mutableListOf()

    private var selectorIndex: Int = 0
        get() {
            field = 0
            visibleViewList.forEachIndexed { index, view ->
                if (view.isSelected) {
                    field = index
                }
            }
            return field
        }

    private val _onChildClickListener = View.OnClickListener {
        val toIndex = visibleViewList.indexOf(it)
        selector(toIndex)
    }

    fun selector(toIndex: Int) {
        val fromIndex = selectorIndex
        if (_selector(fromIndex, toIndex)) {
            selectIndex = toIndex
            itemSelector.onSelectIndexChange?.invoke(fromIndex, toIndex)
        }
    }

    private fun _selector(fromIndex: Int, toIndex: Int): Boolean {
        visibleViewList[fromIndex].isSelected = false
        visibleViewList[toIndex].isSelected = true
        return true
    }

    fun selectDefault() {
        itemSelector.onSelectIndexChange?.invoke(0, 0)
    }

    fun install(
        viewGroup: ViewGroup,
        callback: ItemSelectorCallback.() -> Unit = {}
    ): ItemSelector {
        selectIndex = -1
        parent = viewGroup
        itemSelector.callback()
        updateVisibleItems()
        updateClickListeners()
        return this
    }

    private fun updateClickListeners() {
        parent?.apply {
            for (i in 0 until childCount) {
                getChildAt(i).setOnClickListener(_onChildClickListener)
            }
        }
    }

    fun updateVisibleItems() {
        visibleViewList.clear()
        parent?.apply {
            for (i in 0 until childCount) {
                getChildAt(i)?.let {
                    if (it.visibility == View.VISIBLE) {
                        visibleViewList.add(it)
                    }
                }
            }
        }
    }

}

typealias FromToParam = (fromIndex: Int, toIndex: Int) -> Unit

open class ItemSelectorCallback {
    var onSelectIndexChange: FromToParam? = null
}
