package com.vetalitet.mytestcoolapp.components.imageviews

sealed class PlayButtonState {
    object Loading : PlayButtonState()
    object Playing : PlayButtonState()
    object Pause : PlayButtonState()
}
