package com.vetalitet.mytestcoolapp.components.tabs.delegate

import android.util.SparseArray
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.ui.adapters.PagerViewAdapter
import com.vetalitet.mytestcoolapp.ui.fragments.shazam.RVScroller
import com.vetalitet.mytestcoolapp.utils.updateOpacity
import kotlin.math.max

class ViewPagerToolbarDelegate(
    private val viewPager: ViewPager2, private val activity: AppCompatActivity
) : ViewPager2.OnPageChangeCallback() {

    companion object {
        fun install(
            viewPager: ViewPager2,
            activity: AppCompatActivity
        ) {
            ViewPagerToolbarDelegate(viewPager, activity)
        }
    }

    //private var fromPosition = 0
    private var viewPagerScrollState = 0
    private val scrollPositions by lazy {
        val initialCapacity = viewPager.adapter?.itemCount ?: 0
        val array = SparseArray<Item>(initialCapacity)
        for (i in 0..initialCapacity) {
            array.put(i, Item(0, 0))
        }
        array
    }

    init {
        viewPager.registerOnPageChangeCallback(this)
    }

    override fun onPageScrollStateChanged(state: Int) {
        super.onPageScrollStateChanged(state)
        viewPagerScrollState = state
    }

    private fun getMax(fromPosition: Int, toPosition: Int): Int {
        val from = scrollPositions[fromPosition]
        val to = scrollPositions[toPosition]
        return max(from.opacity, to.opacity)
    }

    override fun onPageScrolled(toPosition: Int, positionOffset: Float, positionOffsetPixels: Int) {
        super.onPageScrolled(toPosition, positionOffset, positionOffsetPixels)
        if (positionOffset <= 0) return

        val fromPosition = viewPager.currentItem
        if (toPosition < fromPosition) {
            val maxValue = getMax(fromPosition, toPosition)
            val koef =
                when {
                    scrollPositions[fromPosition].opacity > scrollPositions[toPosition].opacity -> positionOffset
                    scrollPositions[fromPosition].opacity == scrollPositions[toPosition].opacity -> 1f
                    else -> 1 - positionOffset

                }
            val opacity = (maxValue * koef).toInt()
            activity.updateOpacity(opacity)
        } else {
            val maxValue = getMax(fromPosition, toPosition + 1)
            val koef =
                when {
                    scrollPositions[fromPosition].opacity > scrollPositions[toPosition + 1].opacity -> 1 - positionOffset
                    scrollPositions[fromPosition].opacity == scrollPositions[toPosition + 1].opacity -> 1f
                    else -> positionOffset
                }
            val opacity = (maxValue * koef).toInt()
            activity.updateOpacity(opacity)
        }
    }

    override fun onPageSelected(toPosition: Int) {
        super.onPageSelected(toPosition)
        setUpScroll(toPosition)
    }

    private fun setUpScroll(position: Int) {
        val selectedFragment = (viewPager.adapter as PagerViewAdapter).fragments[position]
        selectedFragment?.view?.findViewById<RecyclerView>(R.id.recyclerView)?.let { recyclerView ->
            val initPos = if (scrollPositions[position] == null) {
                0
            } else {
                scrollPositions[position].scroll
            }
            val scroller = RVScroller.Builder(activity, initPos) { scroll, opacity ->
                scrollPositions.put(position, Item(scroll, opacity))
            }
            recyclerView.addOnScrollListener(scroller.build())
        }
    }

    data class Item(val scroll: Int, val opacity: Int)

}
