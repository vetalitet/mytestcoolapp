package com.vetalitet.mytestcoolapp.components.borderimage

import android.graphics.Bitmap
import android.graphics.BitmapShader
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.ColorFilter
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.Rect
import android.graphics.Shader
import android.graphics.drawable.Drawable

class BorderDrawable(private val bitmap: Bitmap) : Drawable() {

    private val strokeWidthConst = 10f

    private val matrix = Matrix()
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val borderPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = Color.BLACK
        strokeWidth = strokeWidthConst
    }

    private var width = 0
    private var height = 0
    private var centerX = 0f
    private var centerY = 0f
    private var imageRadius = 0f
    private var borderRadius = 0f
    private var bitmapWidth = 0;
    private var bitmapHeight = 0;

    override fun onBoundsChange(bounds: Rect) {
        super.onBoundsChange(bounds)
        bitmapWidth = bitmap.width
        bitmapHeight = bitmap.height

        width = bounds.width()
        height = bounds.height()

        centerX = width / 2.toFloat()
        centerY = width / 2.toFloat()

        imageRadius = width / 2.toFloat() - strokeWidthConst
        borderRadius = width / 2.toFloat() - strokeWidthConst / 2

        updateShaderMatrix()

        val shader = BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
        shader.setLocalMatrix(matrix)
        paint.shader = shader
    }

    private fun updateShaderMatrix() {
        val scale: Float
        var translateX = 0f
        var translateY = 0f

        if (bitmapWidth * height > width * bitmapHeight) {
            scale = height.toFloat() / bitmapHeight
            translateX = Math.round((width / scale - bitmapWidth) / 2f).toFloat()
        } else {
            scale = width.toFloat() / bitmapWidth
            translateY = Math.round((height / scale - bitmapHeight) / 2f).toFloat()
        }

        matrix.setScale(scale, scale)
        matrix.preTranslate(translateX, translateY)
        matrix.postTranslate(strokeWidthConst, strokeWidthConst)
    }

    override fun draw(canvas: Canvas) {
        canvas.drawCircle(centerX, centerY, imageRadius, paint)
        canvas.drawCircle(centerX, centerY, borderRadius, borderPaint)
    }

    override fun setAlpha(alpha: Int) {
        TODO("Not yet implemented")
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        TODO("Not yet implemented")
    }

    override fun getOpacity() = PixelFormat.TRANSLUCENT

}
