package com.vetalitet.mytestcoolapp.components.imageviews

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Animatable
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.DrawableContainer
import android.graphics.drawable.StateListDrawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.vetalitet.mytestcoolapp.R

open class PlayButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    companion object {
        val STATE_PLAY = intArrayOf(R.attr.state_playing)
        val STATE_LOADING = intArrayOf(R.attr.state_loading)
        val STATE_PAUSED = intArrayOf(R.attr.state_paused)
    }

    var playButtonState: PlayButtonState = PlayButtonState.Pause
    var stateListDrawable: StateListDrawable? = null

    init {
        val attributes = context.obtainStyledAttributes(
            attrs, R.styleable.PlayButton, defStyleAttr, 0
        )

        backgroundTintList =
            attributes.getColorStateList(R.styleable.PlayButton_android_backgroundTint)
        val drawable = attributes.getDrawable(R.styleable.PlayButton_android_src)
        if (drawable == null) {
            val draw = ContextCompat.getDrawable(context, R.drawable.ic_playbutton)
            if (draw != null) {
                stateListDrawable = draw as StateListDrawable
            }
        } else if (drawable is StateListDrawable) {
            stateListDrawable = drawable
        } else {
            throw IllegalArgumentException("PlayButton needs a StateListDrawable to be used in android:src")
        }

        val constantState = stateListDrawable?.constantState
        constantState?.let {
            val children = (it as DrawableContainer.DrawableContainerState).children
            children.forEach { drawable ->
                drawable?.let {
                    if (drawable is Animatable) {
                        (drawable as AnimatedVectorDrawable).start()
                    }
                }
            }
        }

        setImageDrawable(stateListDrawable)

        scaleType = ScaleType.CENTER_INSIDE

        attributes.recycle()
    }

    override fun onCreateDrawableState(extraSpace: Int): IntArray {
        val drawableState = super.onCreateDrawableState(extraSpace + 3)
        when (playButtonState) {
            is PlayButtonState.Playing -> mergeDrawableStates(drawableState, STATE_PLAY)
            is PlayButtonState.Pause -> mergeDrawableStates(drawableState, STATE_PAUSED)
            is PlayButtonState.Loading -> mergeDrawableStates(drawableState, STATE_LOADING)
        }
        return drawableState
    }

    fun loading() {
        if (drawable is Animatable) {
            (drawable as AnimatedVectorDrawable).start()
        }
        playButtonState = PlayButtonState.Loading
        refreshDrawableState()
    }

    fun play() {
        playButtonState = PlayButtonState.Playing
        refreshDrawableState()
    }

    fun pause() {
        playButtonState = PlayButtonState.Pause
        refreshDrawableState()
    }

    fun setBackgroundTint(color: Int) {
        val drawableFile: Int = if (checkColorFormula(color)) {
            R.drawable.bg_button_fab_dark
        } else {
            R.drawable.bg_button_fab_light
        }
        val drawable = ContextCompat.getDrawable(context, drawableFile)
        background = drawable
        background?.let {
            DrawableCompat.setTint(it, color)
        }
    }

    private fun checkColorFormula(color: Int) =
        1.0 - (Color.blue(color) * 0.114 + (Color.green(color) * 0.587 + Color.red(color) * 0.299)) / 255.0 >= 0.5

}
