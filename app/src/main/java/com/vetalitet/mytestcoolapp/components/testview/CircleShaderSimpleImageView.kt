package com.vetalitet.mytestcoolapp.components.testview

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapShader
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.Shader
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.graphics.toRectF
import com.vetalitet.mytestcoolapp.R

class CircleShaderSimpleImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    private var srcBitmap: Bitmap? = null

    private val rect = Rect()
    private val borderRect = Rect()
    private val mShaderMatrix = Matrix()
    private val avatarPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val borderPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
    }

    private var borderColor: Int = Color.WHITE
    private var borderWidth: Float = 0f
    private var scale = 0f

    private var mBitmapWidth: Int = 0
    private var mBitmapHeight: Int = 0

    init {
        attrs?.let {
            val a = context.obtainStyledAttributes(attrs, R.styleable.CircleShaderSimpleImageView)
            if (a.hasValue(R.styleable.CircleShaderSimpleImageView_rmiv_border_color)) {
                borderColor = a.getColor(
                    R.styleable.CircleShaderSimpleImageView_rmiv_border_color,
                    Color.WHITE
                )
            }
            if (a.hasValue(R.styleable.CircleShaderSimpleImageView_rmiv_border_width)) {
                borderWidth = a.getDimension(R.styleable.CircleShaderSimpleImageView_rmiv_border_width, 0f)
            }
            a.recycle()
        }
        borderPaint.apply {
            strokeWidth = borderWidth
            color = borderColor
        }
    }

    override fun setImageDrawable(drawable: Drawable?) {
        super.setImageDrawable(drawable)
        srcBitmap = (drawable as BitmapDrawable).bitmap
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val size = resolveDefaultSize(widthMeasureSpec)
        setMeasuredDimension(size, size)
    }

    private fun resolveDefaultSize(spec: Int): Int {
        return when(MeasureSpec.getMode(spec)) {
            MeasureSpec.UNSPECIFIED -> 100
            MeasureSpec.AT_MOST -> MeasureSpec.getSize(spec)
            MeasureSpec.EXACTLY -> MeasureSpec.getSize(spec)
            else -> MeasureSpec.getSize(spec)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        prepareRectValues(w, h)
        calculateBitmapDependentValues(w, h)
        updateShaderMatrix()
    }

    private fun prepareRectValues(w: Int, h: Int) {
        rect.set(0, 0, w, h)
        borderRect.set(rect)
        borderRect.inset((borderWidth / 2).toInt(), (borderWidth / 2).toInt())
    }

    private fun calculateBitmapDependentValues(w: Int, h: Int) {
        srcBitmap?.let { bitmap ->
            val shader = BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
            avatarPaint.shader = shader

            mBitmapWidth = bitmap.width
            mBitmapHeight = bitmap.height
        }

    }

    override fun onDraw(canvas: Canvas) {
        canvas.drawOval(borderRect.toRectF(), borderPaint)

        val bitmapRadius: Float = width / scale / 2f + 0.5f

        canvas.concat(mShaderMatrix)
        canvas.drawCircle(
            (mBitmapWidth / 2).toFloat(),
            (mBitmapHeight / 2).toFloat(),
            bitmapRadius,
            avatarPaint
        )
    }

    private fun updateShaderMatrix() {
        val scale: Float
        var translateX = 0f
        var translateY = 0f

        if (mBitmapWidth * height > width * mBitmapHeight) {
            scale = height.toFloat() / mBitmapHeight
            translateX = Math.round((width / scale - mBitmapWidth) / 2f).toFloat()
        } else {
            scale = width.toFloat() / mBitmapWidth
            translateY = Math.round((height / scale - mBitmapHeight) / 2f).toFloat()
        }

        this.scale = scale

        mShaderMatrix.setScale(scale, scale)
        mShaderMatrix.preTranslate(translateX, translateY)
        mShaderMatrix.postTranslate(borderWidth, borderWidth)
    }

    companion object {
        private const val TAG = "RoundMaskImageView"
    }

}
