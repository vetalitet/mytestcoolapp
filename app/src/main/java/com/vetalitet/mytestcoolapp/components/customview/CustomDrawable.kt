package com.vetalitet.mytestcoolapp.components.customview

import android.graphics.*
import android.graphics.drawable.Drawable
import android.graphics.RectF

class CustomDrawable @JvmOverloads constructor(
    private val color: Int,
    private val borderWidth: Int,
    private val borderRadius: Float
) : Drawable() {

    private val path = Path().apply {
        fillType = Path.FillType.EVEN_ODD
    }
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
    }
    private val rect = RectF()

    override fun onBoundsChange(bounds: Rect) {
        path.reset()
        path.addRect(
            bounds.left.toFloat(),
            bounds.top.toFloat(),
            bounds.right.toFloat(),
            bounds.bottom.toFloat(),
            Path.Direction.CW
        )
        rect.set(
            bounds.left + borderWidth.toFloat(),
            bounds.top + borderWidth.toFloat(),
            bounds.right - borderWidth.toFloat(),
            bounds.bottom - borderWidth.toFloat()
        )
        path.addRoundRect(rect, borderRadius, borderRadius, Path.Direction.CW)
    }

    override fun draw(canvas: Canvas) {
        paint.color = color
        canvas.drawPath(path, paint);
    }

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
    }

    override fun setColorFilter(cf: ColorFilter?) {
        paint.colorFilter = cf
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }

}
