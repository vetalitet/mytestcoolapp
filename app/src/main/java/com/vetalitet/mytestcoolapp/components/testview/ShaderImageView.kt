package com.vetalitet.mytestcoolapp.components.testview

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import com.github.siyamed.shapeimageview.shader.ShaderHelper

abstract class ShaderImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    private var pathHelper: ShaderHelper? = null

    private fun getPathHelper(): ShaderHelper {
        if (pathHelper == null) {
            pathHelper = createImageViewHelper()
        }
        return pathHelper!!
    }

    init {
        getPathHelper().init(context, attrs, defStyleAttr)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        if (getPathHelper().isSquare) {
            super.onMeasure(widthMeasureSpec, widthMeasureSpec)
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        }
    }

    override fun setImageBitmap(bm: Bitmap?) {
        super.setImageBitmap(bm)
        getPathHelper().onImageDrawableReset(drawable)
    }

    override fun setImageDrawable(drawable: Drawable?) {
        super.setImageDrawable(drawable)
        getPathHelper().onImageDrawableReset(drawable)
    }

    override fun setImageResource(resId: Int) {
        super.setImageResource(resId)
        getPathHelper().onImageDrawableReset(drawable)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        getPathHelper().onSizeChanged(w, h)
    }

    override fun onDraw(canvas: Canvas?) {
        if (!getPathHelper().onDraw(canvas)) {
            super.onDraw(canvas)
        }
    }

    protected abstract fun createImageViewHelper(): ShaderHelper

}
