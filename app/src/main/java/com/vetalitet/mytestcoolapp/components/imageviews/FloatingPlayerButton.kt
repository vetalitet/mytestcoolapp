package com.vetalitet.mytestcoolapp.components.imageviews

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

class FloatingPlayerButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    enum class Status {
        NONE, SHOWING, HIDING
    }

    init {

    }

}
