package com.vetalitet.mytestcoolapp.components.customdrawable

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.Rect
import android.graphics.drawable.Drawable
import androidx.annotation.ColorInt

class CircleDrawable(
    private val icon: Drawable,
    @ColorInt private val bgColor: Int): Drawable() {

    companion object {
        private const val DESIRED_SIZE = 80
    }

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = bgColor
    }

    override fun onBoundsChange(bounds: Rect) {
        super.onBoundsChange(bounds)
        icon.setBounds(bounds.centerX() - (DESIRED_SIZE / 2),
            bounds.centerY() - (DESIRED_SIZE / 2),
            (bounds.centerX() - (DESIRED_SIZE / 2)) + DESIRED_SIZE,
            (bounds.centerY() - (DESIRED_SIZE / 2)) + DESIRED_SIZE)
    }

    override fun draw(canvas: Canvas) {
        canvas.drawCircle(bounds.centerX().toFloat(), bounds.centerY().toFloat(),
            bounds.centerX().toFloat(), paint)
        icon.draw(canvas)
    }

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
    }

    override fun setColorFilter(filter: ColorFilter?) {
        paint.colorFilter = filter
    }

    override fun getOpacity() = PixelFormat.TRANSLUCENT

}
