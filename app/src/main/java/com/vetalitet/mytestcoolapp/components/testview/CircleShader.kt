package com.vetalitet.mytestcoolapp.components.testview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import com.github.siyamed.shapeimageview.shader.ShaderHelper

class CircleShader: ShaderHelper() {

    private var center = 0f
    private var bitmapCenterX = 0f
    private var bitmapCenterY = 0f
    private var borderRadius = 0f
    private var bitmapRadius = 0

    override fun init(context: Context?, attrs: AttributeSet?, defStyle: Int) {
        super.init(context, attrs, defStyle)
        square = true
    }

    override fun draw(canvas: Canvas, imagePaint: Paint, borderPaint: Paint) {
        canvas.drawCircle(center, center, borderRadius, borderPaint)
        canvas.save()
        canvas.concat(matrix)
        canvas.drawCircle(bitmapCenterX, bitmapCenterY, bitmapRadius.toFloat(), imagePaint)
        canvas.restore()
    }

    override fun onSizeChanged(width: Int, height: Int) {
        super.onSizeChanged(width, height)
        center = Math.round(viewWidth / 2f).toFloat()
        borderRadius = Math.round((viewWidth - borderWidth) / 2f).toFloat()
    }

    override fun reset() {
        bitmapRadius = 0
        bitmapCenterX = 0f
        bitmapCenterY = 0f
    }

    override fun calculate(
        bitmapWidth: Int,
        bitmapHeight: Int,
        width: Float,
        height: Float,
        scale: Float,
        translateX: Float,
        translateY: Float
    ) {
        bitmapCenterX = Math.round(bitmapWidth / 2f).toFloat()
        bitmapCenterY = Math.round(bitmapHeight / 2f).toFloat()
        bitmapRadius = Math.round(width / scale / 2f + 0.5f)
    }

}