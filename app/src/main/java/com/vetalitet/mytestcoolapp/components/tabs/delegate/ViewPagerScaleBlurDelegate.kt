package com.vetalitet.mytestcoolapp.components.tabs.delegate

import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import kotlin.math.max
import kotlin.math.min

class ViewPagerScaleBlurDelegate(
    private val viewPager: ViewPager2,
    tabLayout: TabLayout,
    private val scrollCallback: (fromPosition: Int, toPosition: Int, positionOffset: Float) -> Unit
) : ViewPager2.OnPageChangeCallback(), TabLayout.OnTabSelectedListener {

    companion object {
        fun install(
            viewPager: ViewPager2,
            tabLayout: TabLayout,
            scrollCallback: (fromPosition: Int, toPosition: Int, positionOffset: Float) -> Unit
        ) {
            ViewPagerScaleBlurDelegate(viewPager, tabLayout, scrollCallback)
        }
    }

    init {
        viewPager.registerOnPageChangeCallback(this)
        tabLayout.addOnTabSelectedListener(this)
    }

    var sum = 0f
    var fromPosition = 0

    override fun onPageScrolled(toPosition: Int, positionOffset: Float, positionOffsetPixels: Int) {
        super.onPageScrolled(toPosition, positionOffset, positionOffsetPixels)
        if (positionOffset <= 0) {
            return
        }

        val fromPosition = viewPager.currentItem
        if (toPosition + positionOffset > sum) {
            val from = min(fromPosition, toPosition)
            val to = max(fromPosition, toPosition + 1)
            scrollCallback(from, to, positionOffset)
        } else {
            val from = max(fromPosition, toPosition + 1)
            val to = min(fromPosition, toPosition)
            scrollCallback(from, to, 1 - (1 - positionOffset))
        }

        sum = toPosition + positionOffset
    }

    override fun onPageSelected(position: Int) {
        super.onPageSelected(position)
        if (fromPosition == 0 && position == 0) return
        if (fromPosition <= 0) {
            scrollCallback(fromPosition, position, 1f)
        } else {
            scrollCallback(fromPosition, position, 0f)
        }
        fromPosition = position
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {

    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {

    }

    override fun onTabReselected(tab: TabLayout.Tab?) {

    }

}
