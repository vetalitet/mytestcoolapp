package com.vetalitet.mytestcoolapp.components.proba

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.components.roundedimage.RoundedDrawable

class ProbaView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    companion object {
        const val DEFAULT_BORDER_COLOR = Color.BLACK
        const val DEFAULT_RADIUS = 0f
        const val DEFAULT_BORDER_WIDTH = 0f
    }

    private var mBorderColor: ColorStateList? = ColorStateList.valueOf(DEFAULT_BORDER_COLOR)
    private var mBorderWidth = DEFAULT_BORDER_WIDTH
    private var mCornerRadius = DEFAULT_RADIUS

    private var mDrawable: Drawable? = null

    init {
        val attributes = context.obtainStyledAttributes(
            attrs,
            R.styleable.ProbaView,
            defStyleAttr,
            0
        )

        mBorderWidth =
            attributes.getDimensionPixelSize(R.styleable.ProbaView_riv_border_width, -1).toFloat()
        if (mBorderWidth < 0) {
            mBorderWidth = DEFAULT_BORDER_WIDTH
        }

        mCornerRadius = attributes.getDimensionPixelSize(R.styleable.ProbaView_riv_corner_radius, -1).toFloat()
        if (mCornerRadius < 0) {
            mCornerRadius = DEFAULT_RADIUS
        }

        mBorderColor = attributes.getColorStateList(R.styleable.ProbaView_riv_border_color)
        if (mBorderColor == null) {
            mBorderColor = ColorStateList.valueOf(DEFAULT_BORDER_COLOR)
        }

        attributes.recycle()
        updateAttrs(mDrawable)
    }

    override fun setImageBitmap(bm: Bitmap?) {
        mDrawable = RoundedDrawable.createFromBitmap(bm)
        updateAttrs(mDrawable)
        super.setImageDrawable(mDrawable)
    }

    override fun setImageDrawable(drawable: Drawable?) {
        mDrawable = RoundedDrawable.createFromDrawable(drawable)
        updateAttrs(mDrawable)
        super.setImageDrawable(mDrawable)
    }

    private fun updateAttrs(drawable: Drawable?) {
        if (drawable == null) {
            return
        }
        if (drawable is RoundedDrawable) {
            drawable
                .setBorderWidth(mBorderWidth)
                .setBorderColor(mBorderColor)
                .setCornerRadius(mCornerRadius)
        } else if (drawable is LayerDrawable) {
            // loop through layers to and set drawable attrs
            val ld = drawable
            var i = 0
            val layers = ld.numberOfLayers
            while (i < layers) {
                updateAttrs(ld.getDrawable(i))
                i++
            }
        }
    }

}
