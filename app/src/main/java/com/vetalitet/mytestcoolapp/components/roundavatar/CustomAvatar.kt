package com.vetalitet.mytestcoolapp.components.roundavatar

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.BitmapShader
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.Shader
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.annotation.ColorInt
import androidx.appcompat.widget.AppCompatImageView
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.components.proba.ProbaView
import kotlin.math.round

class CustomAvatar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    companion object {
        private const val DEFAULT_OVAL_COLOR = Color.GRAY
        private const val DEFAULT_BORDER_COLOR = Color.BLACK
        private const val DEFAULT_TEXT_COLOR = Color.WHITE
        private const val DEFAULT_BORDER_WIDTH = 0f
        private const val DEFAULT_TEXT = ""
        private const val DEFAULT_TEXT_SIZE = 120
    }

    private var ovalColor: ColorStateList = ColorStateList.valueOf(DEFAULT_OVAL_COLOR)
    private var borderColor: ColorStateList = ColorStateList.valueOf(DEFAULT_BORDER_COLOR)
    private var borderWidth = DEFAULT_BORDER_WIDTH

    private var textColor: ColorStateList = ColorStateList.valueOf(DEFAULT_BORDER_COLOR)
    private var textSize = DEFAULT_TEXT_SIZE
    private var text = DEFAULT_TEXT

    private val imagePaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val ovalPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        color = ovalColor.defaultColor
    }
    private val borderPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = borderColor.defaultColor
        strokeWidth = borderWidth
    }
    private val textPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        textAlign = Paint.Align.CENTER
        textSize = textSize
        color = textColor.defaultColor
    }

    private val ovalRect = RectF()
    private val borderRect = RectF()
    private val textBounds = Rect()

    private var srcBitmap: Bitmap? = null
    private var bitmapWidth = 0
    private var bitmapHeight = 0
    private var bitmapRadius = 0f

    private val shaderMatrix = Matrix()

    init {
        val attributes = context.obtainStyledAttributes(
            attrs, R.styleable.CustomAvatar, defStyleAttr, 0)

        borderWidth =
            attributes.getDimensionPixelSize(R.styleable.CustomAvatar_ca_border_width, -1).toFloat()
        if (borderWidth < 0) {
            borderWidth = ProbaView.DEFAULT_BORDER_WIDTH
        }

        borderColor = attributes.getColorStateList(R.styleable.CustomAvatar_ca_border_color)
            ?: ColorStateList.valueOf(DEFAULT_BORDER_COLOR)

        ovalColor = attributes.getColorStateList(R.styleable.CustomAvatar_ca_item_color)
            ?: ColorStateList.valueOf(DEFAULT_OVAL_COLOR)

        text = attributes.getString(R.styleable.CustomAvatar_ca_text) ?: DEFAULT_TEXT

        textColor = attributes.getColorStateList(R.styleable.CustomAvatar_ca_text_color)
            ?: ColorStateList.valueOf(DEFAULT_TEXT_COLOR)

        textSize = attributes.getDimensionPixelSize(R.styleable.CustomAvatar_ca_text_size, DEFAULT_TEXT_SIZE)

        attributes.recycle()
        updatePaints()
    }

    private fun updatePaints() {
        if (borderWidth > 0) {
            borderPaint.strokeWidth = borderWidth
            borderPaint.color = borderColor.defaultColor
        }
        ovalPaint.color = ovalColor.defaultColor

        textPaint.color = textColor.defaultColor
        textPaint.textSize = textSize.toFloat()
        textPaint.getTextBounds(text, 0, text.length, textBounds)

        srcBitmap?.let { bitmap ->
            val shader = BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
            imagePaint.shader = shader
            bitmapWidth = bitmap.width
            bitmapHeight = bitmap.height
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val widthWithoutPadding = measuredWidth - paddingLeft - paddingRight
        val heightWithoutPadding = measuredHeight - paddingTop - paddingBottom

        // set the dimensions
        val size = if (widthWithoutPadding > heightWithoutPadding) {
            heightWithoutPadding
        } else {
            widthWithoutPadding
        }

        setMeasuredDimension(size + paddingLeft + paddingRight, size + paddingTop + paddingBottom)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        ovalRect.set(0f, 0f, w.toFloat(), h.toFloat())
        ovalRect.inset(1f, 1f)
        borderRect.set(0f, 0f, w.toFloat(), h.toFloat())
        borderRect.inset(borderWidth / 2f, borderWidth / 2f)
        updateShaderMatrix()
    }

    override fun onDraw(canvas: Canvas) {
        //canvas.drawOval(ovalRect, ovalPaint)
        val textBottom = round(height / 2f + textBounds.height() / 2f)
        canvas.drawText(text, width / 2f, textBottom, textPaint)

        srcBitmap?.let {
            canvas.save()
            canvas.concat(shaderMatrix)
            canvas.drawCircle(
                (bitmapWidth / 2).toFloat(), (bitmapHeight / 2).toFloat(),
                bitmapRadius - 1.5f, imagePaint)
            canvas.restore()
        }
        if (borderWidth > 0) {
            canvas.drawOval(borderRect, borderPaint)
        }
    }

    override fun setImageDrawable(drawable: Drawable?) {
        super.setImageDrawable(drawable)
        srcBitmap = (drawable as BitmapDrawable).bitmap
    }

    private fun updateShaderMatrix() {
        val scale: Float
        var translateX = 0f
        var translateY = 0f

        val ovalWidth = width - borderWidth * 2
        val ovalHeight = height - borderWidth * 2

        if (bitmapWidth * ovalHeight > ovalWidth * bitmapHeight) {
            scale = ovalHeight / bitmapHeight
            translateX = Math.round((ovalWidth / scale - bitmapWidth) / 2f).toFloat()
        } else {
            scale = ovalWidth / bitmapWidth
            translateY = Math.round((ovalHeight / scale - bitmapHeight) / 2f).toFloat()
        }

        shaderMatrix.setScale(scale, scale)
        shaderMatrix.preTranslate(translateX, translateY)
        shaderMatrix.postTranslate(borderWidth, borderWidth)

        bitmapRadius = width / scale / 2f
    }

    @ColorInt
    fun getOvalColor() = ovalColor.defaultColor

    fun setOvalColor(@ColorInt color: Int) {
        val defaultColor: ColorStateList = ColorStateList.valueOf(color)
        if (ovalColor == defaultColor) return

        ovalColor = defaultColor
        ovalPaint.color = ovalColor.defaultColor
    }

    @ColorInt
    fun getBorderColor() = borderColor.defaultColor

    fun setBorderColor(@ColorInt color: Int) {
        val defaultColor: ColorStateList = ColorStateList.valueOf(color)
        if (borderColor != defaultColor) {
            borderColor = defaultColor
            borderPaint.color = borderColor.defaultColor
            if (borderWidth > 0) {
                invalidate()
            }
        }
    }

    fun getBorderWidth() = borderWidth

    fun setBorderWidth(width: Float) {
        if (borderWidth != width) {
            borderWidth = width
            borderPaint.strokeWidth = borderWidth
            invalidate()
        }
    }

}
