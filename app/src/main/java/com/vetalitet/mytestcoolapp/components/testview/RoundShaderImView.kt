package com.vetalitet.mytestcoolapp.components.testview

import android.content.Context
import android.util.AttributeSet
import com.github.siyamed.shapeimageview.shader.ShaderHelper

class RoundShaderImView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ShaderImageView(context, attrs, defStyleAttr) {

    override fun createImageViewHelper(): ShaderHelper {
        return CircleShader()
    }

}
