package com.vetalitet.mytestcoolapp.components.testview

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.graphics.drawable.toBitmap
import androidx.core.graphics.toRectF
import com.vetalitet.mytestcoolapp.R

class RoundShaderImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    private var mDrawable: Drawable? = null

    private lateinit var srcBitmap: Bitmap
    private lateinit var maskBitmap: Bitmap
    private lateinit var resBitmap: Bitmap

    private val rect = Rect()
    private val borderRect = Rect()
    private val ovalPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.RED
        style = Paint.Style.FILL
    }
    private val borderPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
    }
    private val maskPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
    }

    private var borderColor: Int = Color.WHITE
    private var borderWidth: Float = 0f

    init {
        attrs?.let {
            val a = context.obtainStyledAttributes(attrs, R.styleable.RoundShaderImageView)
            if (a.hasValue(R.styleable.RoundShaderImageView_rsiv_border_color)) {
                borderColor = a.getColor(R.styleable.RoundShaderImageView_rsiv_border_color, Color.WHITE)
            }
            if (a.hasValue(R.styleable.RoundShaderImageView_rsiv_border_width)) {
                borderWidth = a.getDimension(R.styleable.RoundShaderImageView_rsiv_border_width, 0f)
            }
            a.recycle()
        }
        borderPaint.apply {
            strokeWidth = borderWidth
            color = borderColor
        }
    }

    override fun setImageDrawable(drawable: Drawable?) {
        //super.setImageDrawable(drawable)
        mDrawable = drawable
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val size = resolveDefaultSize(widthMeasureSpec)
        setMeasuredDimension(size, size)
    }

    private fun resolveDefaultSize(spec: Int): Int {
        return when(MeasureSpec.getMode(spec)) {
            MeasureSpec.UNSPECIFIED -> 100
            MeasureSpec.AT_MOST -> MeasureSpec.getSize(spec)
            MeasureSpec.EXACTLY -> MeasureSpec.getSize(spec)
            else -> MeasureSpec.getSize(spec)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        rect.set(0, 0, w, h)
        borderRect.set(rect)
        borderRect.inset((borderWidth / 2).toInt(), (borderWidth / 2).toInt())

        prepareBitmaps(w, h)
    }

    private fun prepareBitmaps(w: Int, h: Int) {
        srcBitmap = mDrawable?.toBitmap(w, h, Bitmap.Config.ARGB_8888)!!

        maskBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ALPHA_8)
        resBitmap = maskBitmap.copy(Bitmap.Config.ARGB_8888, true)

        val maskCanvas = Canvas(maskBitmap)
        maskCanvas.drawOval(rect.toRectF(), ovalPaint)

        val resultCanvas = Canvas(resBitmap)
        resultCanvas.drawBitmap(maskBitmap, rect, rect, null)
        resultCanvas.drawBitmap(srcBitmap, rect, rect, maskPaint)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawBitmap(resBitmap, rect, rect, null)
        if (borderWidth > 0) {
            canvas.drawOval(borderRect.toRectF(), borderPaint)
        }
    }

    companion object {
        private const val TAG = "RoundShaderImageView"
    }

}
