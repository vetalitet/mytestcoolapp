package com.vetalitet.mytestcoolapp.components.tabs.delegate

interface ViewPagerDelegate {

    companion object {
        const val SCROLL_STATE_IDLE = 0
        const val SCROLL_STATE_DRAGGING = 1
        const val SCROLL_STATE_SETTLING = 2
    }

    fun onGetCurrentItem(): Int
    fun onSetCurrentItem(fromIndex: Int, toIndex: Int)

}
