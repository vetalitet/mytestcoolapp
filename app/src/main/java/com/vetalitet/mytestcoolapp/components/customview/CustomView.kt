package com.vetalitet.mytestcoolapp.components.customview

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import com.vetalitet.mytestcoolapp.R

class CustomView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    private var customDrawable: CustomDrawable

    init {
        setWillNotDraw(false)
        customDrawable = CustomDrawable(resources.getColor(R.color.drawable_bg_color), 25, 400f)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        customDrawable.setBounds(0, 0, w, h)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        customDrawable.draw(canvas)
    }

}
