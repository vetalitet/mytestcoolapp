package com.vetalitet.mytestcoolapp.components

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapShader
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.Shader
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.graphics.toRect
import kotlin.math.min

class SwipeableImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    private val bgPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val itemPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        color = Color.BLUE
    }

    private var srcBitmap: Bitmap? = null
    private lateinit var overlayBitmap: Bitmap
    private var itemSize = 0
    private var scale = 0f

    private val bgRect = RectF()
    private val itemRect = RectF()
    private val mShaderMatrix = Matrix()

    private var mBitmapWidth: Int = 0
    private var mBitmapHeight: Int = 0

    var initialPositionX = 0f
    var initialPositionY = 0f

    override fun setImageDrawable(drawable: Drawable?) {
        //super.setImageDrawable(drawable)
        srcBitmap = (drawable as BitmapDrawable).bitmap
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        prepareRectValues(w, h)
        calculateBitmapDependentValues()
        updateShaderMatrix()
        handleBitmap()
    }

    private fun handleBitmap() {
        srcBitmap?.let {
            overlayBitmap = it.copy(Bitmap.Config.ARGB_8888, true)
            val canvas = Canvas(overlayBitmap)
            canvas.concat(mShaderMatrix)
            canvas.drawRect(0f, 0f, mBitmapWidth.toFloat(), mBitmapHeight.toFloat(), bgPaint)
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        /*canvas.save()
        canvas.concat(mShaderMatrix)
        canvas.drawRect(0f, 0f, mBitmapWidth.toFloat(), mBitmapHeight.toFloat(), bgPaint)
        canvas.restore()*/

        //canvas.drawRect(itemRect, itemPaint)

        canvas.drawBitmap(overlayBitmap, itemRect.toRect(), itemRect, itemPaint)
    }

    private fun prepareRectValues(w: Int, h: Int) {
        bgRect.set(0f, 0f, w.toFloat(), h.toFloat())
        itemSize = min(w, h) / 2
        itemRect.set(0f, 0f, itemSize.toFloat(), itemSize.toFloat())
    }

    private fun calculateBitmapDependentValues() {
        srcBitmap?.let { bitmap ->
            val shader = BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
            bgPaint.shader = shader

            mBitmapWidth = bitmap.width
            mBitmapHeight = bitmap.height
        }

    }

    private fun updateShaderMatrix() {
        val scale: Float
        var translateX = 0f
        var translateY = 0f

        if (mBitmapWidth * height > width * mBitmapHeight) {
            scale = height.toFloat() / mBitmapHeight
            translateX = Math.round((width / scale - mBitmapWidth) / 2f).toFloat()
        } else {
            scale = width.toFloat() / mBitmapWidth
            translateY = Math.round((height / scale - mBitmapHeight) / 2f).toFloat()
        }

        this.scale = scale
        mShaderMatrix.setScale(scale, scale)
        mShaderMatrix.preTranslate(translateX, translateY)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val posX = event.x
        val posY = event.y
        Log.d("TestTouchActivity", event.action.toString())
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (itemRect.contains(posX, posY)) {
                initialPositionX = posX
                initialPositionY = posY
            }
            return true
        } else if (event.action == MotionEvent.ACTION_MOVE) {
            val dx = event.x - initialPositionX
            val dy = event.y - initialPositionY

            val newLeft = if (itemRect.left + dx + itemSize >= width.toFloat()) {
                width - itemSize.toFloat()
            } else {
                if ((itemRect.left + dx) >= 0) {
                    itemRect.left + dx
                } else {
                    0f
                }
            }
            val newRight = if ((newLeft + itemSize) <= width) newLeft + itemSize else width.toFloat()
            val newTop = if (itemRect.top + dy + itemSize >= height.toFloat()) {
                height - itemSize.toFloat()
            } else {
                if ((itemRect.top + dy) >= 0) {
                    itemRect.top + dy
                } else {
                    0f
                }
            }
            val newBottom = if ((newTop + itemSize) <= height) newTop + itemSize else height.toFloat()
            if (newLeft > 0 || newRight < width || newTop > 0 || newBottom < height) {
                itemRect.set(newLeft, newTop, newRight, newBottom)
                initialPositionX = posX
                initialPositionY = posY
                invalidate()
            }
            return true
        }
        return super.onTouchEvent(event)
    }

}
