package com.vetalitet.mytestcoolapp.components.tabs.delegate

import androidx.viewpager2.widget.ViewPager2
import com.vetalitet.mytestcoolapp.components.tabs.CustomTabLayout
import kotlin.math.absoluteValue

class ViewPager2Delegate(
    private val viewPager: ViewPager2, private val customTabLayout: CustomTabLayout
) : ViewPagerDelegate,
    ViewPager2.OnPageChangeCallback() {

    companion object {
        fun install(
            viewPager: ViewPager2,
            customTabLayout: CustomTabLayout
        ) {
            ViewPager2Delegate(viewPager, customTabLayout)
        }
    }

    init {
        viewPager.offscreenPageLimit = viewPager.adapter?.itemCount ?: 0
        viewPager.registerOnPageChangeCallback(this)
        customTabLayout.setupWithViewPager(this)
    }

    override fun onGetCurrentItem(): Int {
        return viewPager.currentItem
    }

    override fun onSetCurrentItem(fromIndex: Int, toIndex: Int) {
        viewPager.setCurrentItem(toIndex, (toIndex - fromIndex).absoluteValue <= 1)
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        val itemCount = viewPager.adapter?.itemCount ?: 0
        if (position < itemCount - 1) {
            customTabLayout.onPageScrolled(position, positionOffset, positionOffsetPixels)
            customTabLayout.onPageScrolledTabMove(position, positionOffset, positionOffsetPixels)
        }
    }

    override fun onPageScrollStateChanged(state: Int) {
        super.onPageScrollStateChanged(state)
        customTabLayout.onPageScrollStateChanged(state)
    }

    override fun onPageSelected(position: Int) {
        super.onPageSelected(position)
        customTabLayout.onPageSelected(position)
    }

}
