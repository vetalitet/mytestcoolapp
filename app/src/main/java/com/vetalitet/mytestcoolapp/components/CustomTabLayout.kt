package com.vetalitet.mytestcoolapp.components

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.tabs.TabLayout
import java.lang.reflect.Field

class CustomTabLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : TabLayout(context, attrs, defStyleAttr) {

    companion object {
        const val SCROLLABLE_TAB_MIN_WIDTH = "scrollableTabMinWidth"
    }

    init {
        val field: Field
        try {
            field = TabLayout::class.java.getDeclaredField(SCROLLABLE_TAB_MIN_WIDTH)
            field.isAccessible = true
            field.set(this, 50)
        } catch (e: NoSuchFieldException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }
    }

}
