package com.vetalitet.mytestcoolapp.components.views.detailsview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.Log
import android.view.View

class DetailsView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var minSize = 0
    private var mBackgroundDrawable: Drawable? = null

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.BLACK
        style = Paint.Style.STROKE
    }

    companion object {
        private const val TAG = "DetailsView"
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        minSize = if (w < h) w else h
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        Log.d(TAG, "W: $width, H: $height")
        canvas.drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val measuredWidth = reconcileSize(minSize, widthMeasureSpec)
        val measuredHeight = reconcileSize(minSize, heightMeasureSpec)

        Log.d(TAG, "Size: ($measuredWidth, $measuredHeight)")

        setMeasuredDimension(measuredWidth, measuredHeight)
    }

    override fun setBackground(background: Drawable?) {
        mBackgroundDrawable = BackgroundDrawable.fromDrawable(background)
        super.setBackground(mBackgroundDrawable)
        updateDrawableAttrs()
    }

    private fun updateDrawableAttrs() {
    }

    private fun reconcileSize(contentSize: Int, measureSpec: Int): Int {
        val mode = MeasureSpec.getMode(measureSpec)
        val specSize = MeasureSpec.getSize(measureSpec)
        return when (mode) {
            MeasureSpec.EXACTLY -> specSize
            MeasureSpec.AT_MOST -> if (contentSize < specSize) {
                contentSize
            } else {
                specSize
            }
            MeasureSpec.UNSPECIFIED -> contentSize
            else -> contentSize
        }
    }

}
