package com.vetalitet.mytestcoolapp.components.tabs

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ViewConfiguration
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.OverScroller
import androidx.core.view.GestureDetectorCompat
import androidx.core.view.ViewCompat
import androidx.core.view.marginBottom
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import androidx.core.view.marginTop
import com.vetalitet.mytestcoolapp.R
import com.vetalitet.mytestcoolapp.components.tabs.delegate.ViewPagerDelegate
import com.vetalitet.mytestcoolapp.utils.clamp
import com.vetalitet.mytestcoolapp.utils.viewDrawWidth
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

class CustomTabLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ViewGroup(context, attrs, defStyleAttr) {

    private var viewPagerDelegate: ViewPagerDelegate? = null

    // scroll
    var touchSlop: Int = 0
    var minFlingVelocity = 0
    var maxFlingVelocity = 0

    var tabIndicatorAnimationDuration = 240L

    private val needScroll: Boolean
        get() = true

    private val maxWidth: Int
        get() = _childAllWidthSum + paddingLeft + paddingRight

    private val minScrollX: Int
        get() = 0

    private val maxScrollX: Int
        get() = maxWidth - measuredWidth + paddingStart + paddingEnd

    private var _childAllWidthSum = 0
    private var viewPagerScrollState = 0
    private var scrollProgress = 0

    private val currentItemIndex: Int
        get() = itemSelector.selectIndex

    private val isAnimatorStart: Boolean
        get() = scrollAnimator.isStarted

    var tabIndicator: TabIndicator = TabIndicator(this)
        set(value) {
            field = value
            //field.initAttribute(context, attributeSet)
        }

    private val _overScroller: OverScroller by lazy {
        OverScroller(context)
    }

    private val scrollAnimator: ValueAnimator by lazy {
        ValueAnimator().apply {
            interpolator = LinearInterpolator()
            duration = tabIndicatorAnimationDuration
            addUpdateListener {
                tabIndicator.positionOffset = it.animatedValue as Float
                invalidate()
            }
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationCancel(animation: Animator?) {
                }

                override fun onAnimationEnd(animation: Animator?) {
                    tabIndicator.currentIndex = itemSelector.selectIndex
                    tabIndicator.targetIndex = tabIndicator.currentIndex
                    tabIndicator.positionOffset = 0f
                }
            })
        }
    }

    val itemSelector: ItemSelector by lazy {
        ItemSelector().install(this) {
            onSelectIndexChange = { fromIndex, toIndex ->
                animateToItem(fromIndex, toIndex)
                //scrollToCenter(toIndex)
                invalidate()
                viewPagerDelegate?.onSetCurrentItem(fromIndex, toIndex)
            }
        }
    }

    private fun scrollToCenter(index: Int) {
        val childCenterX = tabIndicator.getChildCenterX(index)
        val viewDrawCenterX = paddingLeft + viewDrawWidth / 2
        val dx = if (childCenterX > viewDrawCenterX) {
            childCenterX - viewDrawCenterX - scrollX
        } else {
            -scrollX
        }
        startScroll(dx)
    }

    private fun startScroll(dv: Int) {
        _overScroller.abortAnimation()
        _overScroller.startScroll(scrollX, scrollY, dv, 0)
        ViewCompat.postInvalidateOnAnimation(this)
    }

    private fun animateToItem(fromIndex: Int, toIndex: Int) {
        scrollAnimator.cancel()

        Log.d("TAG_TTT", "animateToItem: fromIndex - $fromIndex, toIndex - $toIndex, offset - ${tabIndicator.positionOffset}")

        tabIndicator.currentIndex = fromIndex
        tabIndicator.targetIndex = toIndex

        scrollAnimator.setFloatValues(tabIndicator.positionOffset, 1f)
        scrollAnimator.start()
    }

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomTabLayout)
        tabIndicator.initAttributes(context, attrs)
        typedArray.recycle()
        setWillNotDraw(false)
    }

    private val gestureDetector: GestureDetectorCompat by lazy {
        GestureDetectorCompat(context, object : GestureDetector.SimpleOnGestureListener() {
            override fun onScroll(
                e1: MotionEvent?,
                e2: MotionEvent?,
                distanceX: Float,
                distanceY: Float
            ): Boolean {
                var handle = false
                val absX = abs(distanceX)
                if (absX > touchSlop) {
                    handle = onScrollChange(distanceX)
                }
                return handle
            }

            override fun onFling(
                e1: MotionEvent?,
                e2: MotionEvent?,
                velocityX: Float,
                velocityY: Float
            ): Boolean {
                val absX = abs(velocityX)
                if (absX > minFlingVelocity) {
                    onFlingChange(velocityX)
                }
                return true
            }
        })
    }

    init {
        val vc = ViewConfiguration.get(context)
        minFlingVelocity = vc.scaledMinimumFlingVelocity
        maxFlingVelocity = vc.scaledMaximumFlingVelocity
    }

    private fun onFlingChange(velocity: Float) {
        if (needScroll) {
            startFling(-velocity.toInt(), maxWidth)
        }
    }

    fun startFling(velocity: Int, max: Int) {

        fun velocity(velocity: Int): Int {
            return if (velocity > 0) {
                clamp(velocity, minFlingVelocity, maxFlingVelocity)
            } else {
                clamp(velocity, -maxFlingVelocity, -minFlingVelocity)
            }
        }

        val v = velocity(velocity)

        _overScroller.abortAnimation()

        _overScroller.fling(
            scrollX,
            scrollY,
            v,
            0,
            0,
            max,
            0,
            0,
            measuredWidth,
            0
        )
        postInvalidate()
    }

    private fun onScrollChange(distanceX: Float): Boolean {
        if (needScroll) {
            parent.requestDisallowInterceptTouchEvent(true)
            scrollBy(distanceX.toInt(), 0)
            return true
        }
        return false
    }

    override fun scrollTo(x: Int, y: Int) {
        when {
            x > maxScrollX -> super.scrollTo(maxScrollX, 0)
            x < minScrollX -> super.scrollTo(minScrollX, 0)
            else -> super.scrollTo(x, 0)
        }
    }

    override fun computeScroll() {
        if (_overScroller.computeScrollOffset()) {
            scrollTo(_overScroller.currX, _overScroller.currY)
            invalidate()
            if (_overScroller.currX < minScrollX || _overScroller.currX > maxScrollX) {
                _overScroller.abortAnimation()
            }
        }
    }

    fun setupWithViewPager(viewPagerDelegate: ViewPagerDelegate) {
        this.viewPagerDelegate = viewPagerDelegate
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        var childMaxWidth = 0
        var childMaxHeight = 0
        _childAllWidthSum = 0

        val children = itemSelector.visibleViewList

        children.forEach { child ->
            val lp = child.layoutParams as CustomLayoutParams
            measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0)
            // NOTE! After measuring padding is already included in child.measuredWidth/child.measuredHeight
            childMaxWidth += child.measuredWidth + lp.leftMargin + lp.rightMargin
            childMaxHeight = max(
                childMaxHeight,
                child.measuredHeight + lp.topMargin + lp.bottomMargin
            )
            _childAllWidthSum += child.measuredWidth
        }

        val viewGroupWidth = paddingStart + paddingEnd + marginStart + marginEnd + childMaxWidth
        val viewGroupHeight = paddingTop + paddingBottom + marginTop + marginBottom + childMaxHeight

        setMeasuredDimension(
            measureWidth(widthMeasureSpec, viewGroupWidth),
            measureHeight(heightMeasureSpec, viewGroupHeight)
        )
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        if (changed) {
            var left = paddingStart
            for (i in 0 until childCount) {
                val child = getChildAt(i)
                val lp = child.layoutParams as CustomLayoutParams
                left += lp.leftMargin
                val childBottom = measuredHeight - paddingBottom
                child.layout(
                    left,
                    childBottom - child.measuredHeight - child.marginBottom,
                    left + child.measuredWidth,
                    childBottom - child.marginBottom
                )
                left += child.measuredWidth + lp.rightMargin
            }
            if (itemSelector.selectIndex < 0) {
                itemSelector.selectDefault()
            }
        }
    }

    private fun measureWidth(measureSpec: Int, viewGroupWidth: Int): Int {
        var result = 0
        val specMode = MeasureSpec.getMode(measureSpec)
        val specSize = MeasureSpec.getSize(measureSpec)
        when (specMode) {
            MeasureSpec.UNSPECIFIED -> result = specSize
            MeasureSpec.AT_MOST ->                 //Compare the remaining width with the value of all subviews + padding, and take the smaller one as the width of ViewGroup
                result = Math.min(viewGroupWidth, specSize)
            MeasureSpec.EXACTLY -> result = specSize
        }
        return result
    }

    private fun measureHeight(measureSpec: Int, viewGroupHeight: Int): Int {
        var result = 0
        val specMode = MeasureSpec.getMode(measureSpec)
        val specSize = MeasureSpec.getSize(measureSpec)
        when (specMode) {
            MeasureSpec.UNSPECIFIED -> result = specSize
            MeasureSpec.AT_MOST ->                 // Compare the remaining height with the value of all subviews + padding, and take the smaller one as the height of ViewGroup
                result = Math.min(viewGroupHeight, specSize)
            MeasureSpec.EXACTLY -> result = specSize
        }
        return result
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        if (needScroll) {
            return super.onInterceptTouchEvent(ev) || gestureDetector.onTouchEvent(ev)
        }
        return super.onInterceptTouchEvent(ev)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (needScroll) {
            gestureDetector.onTouchEvent(event)
            if (event.actionMasked == MotionEvent.ACTION_CANCEL ||
                event.actionMasked == MotionEvent.ACTION_UP
            ) {
                parent.requestDisallowInterceptTouchEvent(false)
            } else if (event.actionMasked == MotionEvent.ACTION_DOWN) {

            }
            return true
        } else {
            return super.onTouchEvent(event)
        }
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        Log.d("onPageScrolled", "CustomTabLayout draw")
        tabIndicator.draw(canvas)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        Log.d("onPageScrolled", "CustomTabLayout onDraw")
    }

    override fun checkLayoutParams(p: LayoutParams?): Boolean {
        return p is CustomLayoutParams
    }

    override fun generateDefaultLayoutParams(): LayoutParams {
        return CustomLayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )
    }

    override fun generateLayoutParams(p: LayoutParams?): LayoutParams {
        return p?.run { CustomLayoutParams(p) } ?: generateDefaultLayoutParams()
    }

    override fun generateLayoutParams(attrs: AttributeSet?): LayoutParams {
        return CustomLayoutParams(context, attrs)
    }

    fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        if (isAnimatorStart) return

        val currentItem = viewPagerDelegate?.onGetCurrentItem() ?: 0
        if (position < currentItem) {
            if (viewPagerScrollState == ViewPagerDelegate.SCROLL_STATE_DRAGGING) {
                tabIndicator.targetIndex = min(currentItem, position)
            }
            tabIndicator.positionOffset = 1 - positionOffset
            invalidate()
        } else {
            if (viewPagerScrollState == ViewPagerDelegate.SCROLL_STATE_DRAGGING) {
                tabIndicator.targetIndex = max(currentItem, position + 1)
            }
            tabIndicator.positionOffset = positionOffset
            invalidate()
        }
    }

    var sum = 0f

    fun onPageScrolledTabMove(toPosition: Int, positionOffset: Float, positionOffsetPixels: Int) {
        if (positionOffset <= 0) {
            scrollProgress = scrollX
            return
        }

        val fromPosition = viewPagerDelegate?.onGetCurrentItem() ?: 0
        if (toPosition + positionOffset > sum) {
            updateRightToLeftScroll(max(fromPosition, toPosition + 1), positionOffset)
        } else {
            updateLeftToRightScroll(min(fromPosition, toPosition), positionOffset)
        }

        sum = toPosition + positionOffset
    }

    private fun updateRightToLeftScroll(index: Int, positionOffset: Float) {
        val childCenterX = tabIndicator.getChildCenterX(index)
        val viewDrawCenterX = paddingLeft + viewDrawWidth / 2
        if (childCenterX > viewDrawCenterX) {
            val diff = childCenterX - viewDrawCenterX
            scrollTo(scrollProgress + (diff * positionOffset).toInt(), 0)
        }
    }

    private fun updateLeftToRightScroll(index: Int, positionOffset: Float) {
        val childCenterX = tabIndicator.getChildCenterX(index)
        val viewDrawCenterX = paddingLeft + viewDrawWidth / 2 + scrollProgress
        if (childCenterX < viewDrawCenterX) {
            val diff = viewDrawCenterX - childCenterX
            scrollTo(scrollProgress - (diff * (1 - positionOffset)).toInt(), 0)
        }
    }

    fun onPageScrollStateChanged(state: Int) {
        viewPagerScrollState = state
    }

    fun onPageSelected(index: Int) {
        if (currentItemIndex == index) {
            scrollToCenter(index)
            return
        }
        itemSelector.selector(index)
    }

    class CustomLayoutParams : MarginLayoutParams {
        constructor(source: LayoutParams) : super(source)
        constructor(width: Int, height: Int) : super(width, height)
        constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    }

}
